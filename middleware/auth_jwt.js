'use strict';
const jwt = require('jsonwebtoken');
const { logger_system } = require('../helper');

const auth_jwt_bearer = async function (req, res, next) {
    logger_system(req, res);
    const { authorization } = req.headers;
    if (!authorization) return res.status(401).json({ message: 'Unauthorized!' });

    return new Promise((resolve, reject) => {
        // const { authorization } = req.headers;
        // if (!authorization) return res.status(401).json({ message: 'Unauthorized!' });

        const authSplit = authorization.split(' ')
        const [authType, authToken] = [
            authSplit[0],
            authSplit[1]
        ]

        if (authType !== 'Bearer') return res.status(401).end();

        return jwt.verify(authToken, process.env.APP_KEY, function (err, decode) {
            if (err) return res.status(401).end();
            return resolve(decode);
        })
    });
}

const auth_jwt = function (req, res, next) {
    logger_system(req, res);
    const token = req.cookies.token;
    if (!token) return res.status(403).json({ message: 'empty token!' });

    try {
        const auth = jwt.verify(token, process.env.APP_KEY);
        // req.auth = auth;
        return next();
    }
    catch (error) {
        res.clearCookie('token');
        return res.status(403).json({ message: 'invalid token!' });
    }
}

const auth_jwt_decoded = function (token) {
    try {
        const decoded = jwt.verify(token, process.env.APP_KEY);
        if (!decoded) return false;
        return decoded;
    } catch (error) {
        console.log(error.message);
        return false;
    }
}

module.exports = {
    auth_jwt_bearer,
    auth_jwt,
    auth_jwt_decoded,
}