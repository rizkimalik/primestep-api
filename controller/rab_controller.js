'use strict';
const knex = require('../config/connection');
const readXlsxFile = require('read-excel-file/node')
const { response, logger, random_string } = require('../helper');
const { upload_document_file } = require('../helper/file_manager');
const { auth_jwt_bearer } = require('../middleware');

//? rab list
exports.rab_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, package_no } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY no ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM budget_plan
                WHERE package_no='${package_no}'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from budget_plan WHERE package_no='${package_no}' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'rab/rab_list');
    }
}

exports.rab_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { modul, package_no } = req.body;

        const result = await knex.raw(`
            SELECT SUM(expenditure) as total_expenditure, COUNT(*) AS total_component
            FROM budget_plan WHERE modul='${modul}' AND package_no='${package_no}'
        `);
        const doc = await knex('document_upload').where({ package_no, document_for: 'usulan-rab' }).first();
        result[0][0].rab_file = doc ? doc.filename : '';
        response.ok(res, result[0][0]);
    }
    catch (error) {
        response.error(res, error.message, 'rab/rab_detail');
    }
}

exports.rab_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            id,
            no,
            component,
            expenditure,
        } = req.body;

        await knex('budget_plan')
            .update({
                no,
                component,
                expenditure,
                updated_at: knex.fn.now(),
            })
            .where({ id });
        response.ok(res, 'success rab_update');
    }
    catch (error) {
        logger('rab/rab_update', error.message)
    }
}

exports.rab_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('budget_plan').del().where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'rab/rab_destroy');
    }
}



exports.upload_excel_rab = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const excel_file = req.files?.excel_file;
        const { package_no, institution, year, modul } = req.body;

        let contacts = [];
        if (excel_file) {
            let isValid = false;
            await readXlsxFile(Buffer.from(excel_file.data, 'utf-8')).then(async (rows) => {
                //? replace / hapus data if exist
                await knex('budget_plan').del().where({ package_no });
                await knex('document_upload').del().where({ package_no, document_for: 'usulan-rab' });

                //? ambil daftar komponen, baris 3 sampe 7
                if (rows[0][0].substr(0, 24) === 'Rincian Anggaran Belanja' && modul === 'research') {
                    isValid = true;
                    for (let i = 2; i <= 9; i++) {
                        const row = rows[i];
                        contacts.push({
                            no: row[0],
                            component: row[1],
                            expenditure: row[2]
                        });

                        await insert_data_rab({
                            no: row[0],
                            component: row[1],
                            expenditure: row[2],
                            package_no,
                            institution,
                            year,
                            modul,
                        });
                    }
                }

                //? ambil daftar komponen, baris 3 sampe 12
                if (rows[0][0].substr(0, 24) === 'Rincian Anggaran Belanja' && modul === 'startup') {
                    isValid = true;
                    for (let i = 2; i <= 11; i++) {
                        const row = rows[i];
                        contacts.push({
                            no: row[0],
                            component: row[1],
                            expenditure: row[2]
                        });

                        await insert_data_rab({
                            no: row[0],
                            component: row[1],
                            expenditure: row[2],
                            package_no,
                            institution,
                            year,
                            modul,
                        });
                    }
                }

            });

            //? simpan excel & data ke db
            if (isValid) {
                const filename = random_string(5) + '-' + excel_file.name;
                await upload_document_file({ channel: 'usulan-rab', attachment: excel_file, filename });
                await knex('document_upload')
                    .insert([{
                        package_no,
                        document_for: 'usulan-rab',
                        document_name: filename,
                        status: 'RAB',
                        filename,
                        filesize: excel_file.size,
                        created_at: knex.fn.now()
                    }]);
                response.ok(res, contacts);
            }
            else {
                response.error(res, 'Dokumen RAB tidak sesuai.', 'rab/upload_excel_rab');
            }
        }
        else {
            response.error(res, 'Dokumen RAB kosong.', 'rab/upload_excel_rab');
        }
    }
    catch (error) {
        response.error(res, error.message, 'rab/upload_excel_rab');
    }
}

const insert_data_rab = async function ({ no, component, expenditure, package_no, institution, year, modul }) {
    try {
        await knex('budget_plan')
            .insert({
                package_no,
                no,
                component,
                expenditure,
                institution,
                year,
                modul,
                created_at: knex.fn.now(),
            });
        return 'success insert_data_rab'
    }
    catch (error) {
        logger('rab/insert_data_rab', error.message);
    }
}
