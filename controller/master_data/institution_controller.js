const knex = require('../../config/connection');
const response = require('../../helper/json_response');
const { auth_jwt_bearer } = require('../../middleware');

const index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const result = await knex('institution').orderBy('id', 'asc');
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'institution/index');
    }
}

const list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM institution
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from institution ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'institution/list');
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const institution = await knex('institution').where({ id }).first();
        response.ok(res, institution);
    }
    catch (error) {
        response.error(res, error.message, 'institution/show');
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            institution_code,
            institution_name,
        } = req.body;

        await knex('institution')
            .insert([{
                institution_code,
                institution_name,
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        response.error(res, error.message, 'institution/store');
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            institution_code,
            institution_name,
        } = req.body;

        await knex('institution')
            .update({
                institution_code,
                institution_name,
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        response.error(res, error.message, 'institution/update');
    }
}

const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('institution').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'institution/destroy');
    }
}

module.exports = {
    index,
    list,
    show,
    store,
    update,
    destroy,
}