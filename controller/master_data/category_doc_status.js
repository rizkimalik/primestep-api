const knex = require('../../config/connection');
const { response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

exports.index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { modul, status } = req.body;
        const result = await knex('category_doc_status').where({ modul, status });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'category_doc_status/index');
    }
}

exports.docs_uploaded_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { modul, status, method, user_level } = req.body;
        let result = '';
        if (method) {
            result = await knex('category_doc_status')
                .where({ modul, status, upload_document: 1, method })
                .orWhere({ modul, status, upload_document: 1, method: 'All' });
        }
        else if (user_level) {
            if (user_level === 'PIU') {
                result = await knex('category_doc_status').where({ modul, status, upload_document: 1, user_level: 'PIU' });
            } else {
                result = await knex('category_doc_status').where({ modul, status, upload_document: 1, user_level: 'PMU' });
            }
        }
        else {
            result = await knex('category_doc_status').where({ modul, status, upload_document: 1 });
        }
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'category_doc_status/docs_uploaded_list');
    }
}


exports.doc_status_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, modul } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM category_doc_status WHERE modul='${modul}'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from category_doc_status WHERE modul='${modul}' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'category_doc_status/doc_status_list');
    }
}

exports.doc_status_detail = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const result = await knex('category_doc_status').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'category_doc_status/doc_status_detail');
    }
}

exports.doc_status_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            modul,
            status,
            document_name,
            upload_document,
            method,
            review,
            user_level,
            document_section,
        } = req.body;

        await knex('category_doc_status')
            .insert([{
                modul,
                status,
                document_name,
                upload_document,
                method,
                review,
                user_level,
                document_section,
            }]);
        response.ok(res, 'success doc_status_insert');
    }
    catch (error) {
        response.error(res, error.message, 'category_doc_status/doc_status_insert');

    }
}

exports.doc_status_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            modul,
            status,
            document_name,
            upload_document,
            method,
            review,
            user_level,
            document_section,
        } = req.body;

        await knex('category_doc_status')
            .update({
                modul,
                status,
                document_name,
                upload_document,
                method,
                review,
                user_level,
                document_section,
            })
            .where({ id });
        response.ok(res, 'success doc_status_update');
    }
    catch (error) {
        response.error(res, error.message, 'category_doc_status/doc_status_update');
    }
}


exports.doc_status_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('category_doc_status').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'category_doc_status/doc_status_destroy');
    }
}