const knex = require('../../config/connection');
const { response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

exports.pagu_contract_index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { institution, year_alocation, modul } = req.body;
        const result = await knex('pagu_contract').where({ institution, year_alocation, modul }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'pagu_contract/pagu_contract_index');
    }
}

exports.pagu_contract_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, modul } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM pagu_contract WHERE modul='${modul}'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from pagu_contract WHERE modul='${modul}' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'pagu_contract/pagu_contract_list');
    }
}

exports.pagu_contract_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            institution,
            total_alocation,
            year_alocation,
            periode_usulan,
            modul,
        } = req.body;

        await knex('pagu_contract')
            .insert({
                institution,
                total_alocation,
                year_alocation,
                periode_usulan: new Date(periode_usulan),
                modul,
            });
        response.ok(res, 'success pagu_contract_insert');
    }
    catch (error) {
        response.error(res, error.message, 'pagu_contract/pagu_contract_insert');

    }
}

exports.pagu_contract_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            institution,
            total_alocation,
            year_alocation,
            periode_usulan,
            modul,
        } = req.body;

        await knex('pagu_contract')
            .update({
                institution,
                total_alocation,
                year_alocation,
                periode_usulan: new Date(periode_usulan),
                modul,
            })
            .where({ id });
        response.ok(res, 'success pagu_contract_update');
    }
    catch (error) {
        response.error(res, error.message, 'pagu_contract/pagu_contract_update');

    }
}

exports.pagu_contract_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('pagu_contract').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'pagu_contract/pagu_contract_destroy');
    }
}
