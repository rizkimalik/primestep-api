const user_level = require('./user_level_crontroller');
const institution = require('./institution_controller');
const category_package = require('./category_package');
const category_doc_status = require('./category_doc_status');
const procurement_method = require('./procurement_method');
const procurement_review = require('./procurement_review');
const category_focus = require('./category_focus');
const category_framework = require('./category_framework');
const indicator_tkt = require('./indicator_tkt');
const strengthen_component = require('./strengthen_component');
const strengthen_category = require('./strengthen_category');
const pagu_contract = require('./pagu_contract');

module.exports =  {
    user_level,
    institution,
    category_package,
    category_doc_status,
    procurement_method,
    procurement_review,
    category_focus,
    category_framework,
    indicator_tkt,
    strengthen_component,
    strengthen_category,
    pagu_contract,
}