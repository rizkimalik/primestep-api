const knex = require('../../config/connection');
const { response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

exports.strengthen_component_index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const result = await knex('strengthen_component');
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'strengthen_component/strengthen_component_index');
    }
}

exports.strengthen_component_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM strengthen_component
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from strengthen_component ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_component/strengthen_component_list');
    }
}

exports.strengthen_component_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            component,
            unit,
        } = req.body;

        await knex('strengthen_component')
            .insert({
                component,
                unit,
            });
        response.ok(res, 'success strengthen_component_insert');
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_component/strengthen_component_insert');

    }
}

exports.strengthen_component_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            component,
            unit,
        } = req.body;

        await knex('strengthen_component')
            .update({
                component,
                unit,
            })
            .where({ id });
        response.ok(res, 'success strengthen_component_update');
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_component/strengthen_component_update');

    }
}

exports.strengthen_component_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('strengthen_component').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_component/strengthen_component_destroy');
    }
}