const knex = require('../../config/connection');
const { response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

exports.index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const result = await knex('category_package');
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'category_package/index');
    }
}

exports.category_package_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM category_package
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from category_package ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'category_package/category_package_list');
    }
}

exports.category_package_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            category_code,
            category_description,
        } = req.body;

        await knex('category_package')
            .insert({
                category_code,
                category_description,
            });
        response.ok(res, 'success category_package_insert');
    }
    catch (error) {
        response.error(res, error.message, 'category_package/category_package_insert');

    }
}

exports.category_package_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            category_code,
            category_description,
        } = req.body;

        await knex('category_package')
            .update({
                category_code,
                category_description,
            })
            .where({ id });
        response.ok(res, 'success category_package_update');
    }
    catch (error) {
        response.error(res, error.message, 'category_package/category_package_update');

    }
}

exports.category_package_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('category_package').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'category_package/category_package_destroy');
    }
}
