const knex = require('../../config/connection');
const { response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

exports.index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { institution } = req.body;
        const result = await knex('category_framework').where({ institution });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'category_framework/index');
    }
}

exports.category_frameworklist = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM category_framework
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from category_framework ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'category_framework/category_frameworklist');
    }
}

exports.category_framework_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            institution,
            framework,
        } = req.body;

        await knex('category_framework')
            .insert({
                institution,
                framework,
                created_at: knex.fn.now(),
            });
        response.ok(res, 'success category_framework_insert');
    }
    catch (error) {
        response.error(res, error.message, 'category_framework/category_framework_insert');

    }
}

exports.category_framework_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            institution,
            framework,
        } = req.body;

        await knex('category_framework')
            .update({
                institution,
                framework,
                updated_at: knex.fn.now(),
            })
            .where({ id });
        response.ok(res, 'success category_framework_update');
    }
    catch (error) {
        response.error(res, error.message, 'category_framework/category_framework_update');

    }
}

exports.category_framework_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('category_framework').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'category_framework/category_framework_destroy');
    }
}
