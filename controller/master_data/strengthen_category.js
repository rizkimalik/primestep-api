const knex = require('../../config/connection');
const { response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

exports.strengthen_category_index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const result = await knex('strengthen_category');
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'strengthen_category/strengthen_category_index');
    }
}


exports.strengthen_category_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM strengthen_category
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from strengthen_category ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_category/strengthen_category_list');
    }
}

exports.strengthen_category_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            category_header,
            category_code,
            category_name,
        } = req.body;

        await knex('strengthen_category')
            .insert({
                category_header,
                category_code,
                category_name,
            });
        response.ok(res, 'success strengthen_category_insert');
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_category/strengthen_category_insert');

    }
}

exports.strengthen_category_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            category_header,
            category_code,
            category_name,
        } = req.body;

        await knex('strengthen_category')
            .update({
                category_header,
                category_code,
                category_name,
            })
            .where({ id });
        response.ok(res, 'success strengthen_category_update');
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_category/strengthen_category_update');

    }
}

exports.strengthen_category_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('strengthen_category').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_category/strengthen_category_destroy');
    }
}