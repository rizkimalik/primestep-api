'use strict';
const knex = require('../../config/connection');
const { response } = require('../../helper');
// const { auth_jwt_bearer } = require('../../middleware');

exports.category_tkt_index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const result = await knex('category_tkt');
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'indicator_tkt/category_tkt_index');
    }
}

exports.indicator_tkt_index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { category_tkt, tkt_no } = req.body;
        const result = await knex('category_indicator_tkt').where({ category_tkt, tkt_no });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'indicator_tkt/indicator_tkt_index');
    }
}