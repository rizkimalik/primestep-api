const knex = require('../config/connection');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const logger = require('../helper/logger');
const response = require('../helper/json_response');
const { auth_jwt_decoded } = require('../middleware/auth_jwt');


exports.login = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { username, password } = req.body;

        //?check username
        const user = await knex('users').where({ username }).first();
        if (!user) return response.forbidden(res, { value: 'username', message: 'Username not found' }, 'auth/login');

        //?check password
        const checkPassword = await bcrypt.compare(password, user.password)
        if (!checkPassword) return response.forbidden(res, { value: 'password', message: 'Password not match' }, 'auth/login');

        //?login update
        await knex('users').update({ login: 1, aux: 1 }).where({ username });

        //?generate token
        const token = jwt.sign({
            id: user.id,
            username: user.username
        }, process.env.APP_KEY, { expiresIn: 28800 }) //8hours

        //?send token
        user.token = token;
        response.ok(res, user);
    }
    catch (error) {
        response.error(res, error.message, 'auth/login')
    }
}

exports.logout = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { username } = req.body;

        //?login update
        const result = await knex('users').update({ login: 0, aux: 2 }).where({ username });
        if (result) {
            response.ok(res, { message: 'logout success' });
        }
        else {
            response.error(res, 'failed action', 'auth/logout')
        }
    }
    catch (error) {
        response.error(res, error.message, 'auth/logout')
    }
}

exports.check_auth_expired = async function (req, res) {
    if (req.method !== 'POST') return res.status(405).end();
    const { token } = req.body;
    if (!token) return response.ok(res, 'empty token!');

    try {
        const decoded = await auth_jwt_decoded(token);
        if (decoded) {
            await knex('users').update({ login: 1 }).where({ username: decoded.username });
            response.ok(res, decoded);

            const user = await knex('users').where({ username: decoded.username, login_kick: 1 }).first();
            if (user) {
                await knex('users').update({ login: 0, aux: 2 }).where({ username: decoded.username });
                response.error(res, 'auth login kick out', 'auth/check_auth_expired');
            }
        }
        else {
            await knex('users').update({ login: 0, aux: 2 }).where({ username: decoded.username });
            response.error(res, 'token expired', 'auth/check_auth_expired');
        }
    }
    catch (error) {
        logger('auth/check_auth_expired', error);
        return res.status(500).end();
    }
}
