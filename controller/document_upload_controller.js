'use strict';
const knex = require('../config/connection');
const { response, random_string } = require('../helper');
const { upload_document_file } = require('../helper/file_manager');
const { auth_jwt_bearer } = require('../middleware');

exports.insert_docs_upload = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const document_file = req.files?.document_file;
        const {
            package_no,
            document_for,
            document_name,
            document_section,
            status,
            description,
            institution,
            user_upload,
        } = req.body;

        if (document_file) {
            const filename = random_string(5) + '-' + document_file.name;
            await upload_document_file({ channel: document_for, attachment: document_file, filename });
            await knex('document_upload')
                .insert([{
                    package_no,
                    document_for,
                    document_name,
                    document_section: document_section === 'undefined' ? null : document_section,
                    status,
                    filename,
                    filesize: document_file.size,
                    description,
                    institution,
                    user_upload,
                    approved: institution === 'PMU' ? 1 : null,
                    created_at: knex.fn.now()
                }]);
        }
        response.ok(res, 'Uploaded insert_docs_upload');
    }
    catch (error) {
        response.error(res, error.message, 'document_upload/insert_docs_upload');
    }
}

exports.update_docs_upload = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            id,
            description,
        } = req.body;

        await knex('document_upload')
            .update({ description })
            .where({ id });
        response.ok(res, 'success update_docs_upload');
    }
    catch (error) {
        response.error(res, error.message, 'document_upload/update_docs_upload');
    }
}

exports.list_docs_uploaded = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, package_no, document_for, status } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 5;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY approved, id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }
        
        let where_condition = '';
        if (document_for) {
            where_condition = `WHERE package_no='${package_no}' AND document_for='${document_for}' AND status='${status}'`;
        }
        else{
            where_condition = `WHERE package_no='${package_no}' AND status='${status}'`;
        }

        const result = await knex.raw(`
            SELECT * FROM document_upload
            ${where_condition}
            ${filtering}
            ${orderby}
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'document_upload/list_docs_uploaded');
    }
}

exports.delete_document_upload = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('document_upload').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'document_upload/delete_document_upload');
    }
}
