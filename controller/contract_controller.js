'use strict';
const knex = require('../config/connection');
const { response, currency_to_number } = require('../helper');
const { auth_jwt_bearer } = require('../middleware');

exports.contract_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no } = req.body;
        const result = await knex('loan_contract').where({ package_no }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'contract/contract_detail');
    }
}

exports.contract_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            package_no,
            institution,
            contract_no,
            contract_no_piu,
            contract_no_adb,
            contract_vendor,
            contract_date,
            contract_value,
            contract_periode,
            contract_last_date,
        } = req.body;

        if (package_no) {
            const status = await knex('loan_contract').where({ package_no }).first();
            if (status) {
                await knex('loan_contract')
                    .update({
                        contract_no,
                        institution,
                        contract_no_piu,
                        contract_no_adb,
                        contract_vendor,
                        contract_date: contract_date === '' ? knex.fn.now() : contract_date,
                        contract_value: currency_to_number(contract_value),
                        contract_periode,
                        contract_last_date: contract_last_date === '' ? knex.fn.now() : contract_last_date,
                        updated_at: knex.fn.now()
                    })
                    .where({ package_no });

            } else {
                await knex('loan_contract')
                    .insert([{
                        package_no,
                        institution,
                        contract_no,
                        contract_no_piu,
                        contract_no_adb,
                        contract_vendor,
                        contract_date: contract_date === '' ? knex.fn.now() : contract_date,
                        contract_value: currency_to_number(contract_value),
                        contract_periode,
                        contract_last_date: contract_last_date === '' ? knex.fn.now() : contract_last_date,
                        created_at: knex.fn.now()
                    }]);
            }
        }
        response.ok(res, 'success contract_update');
    }
    catch (error) {
        response.error(res, error.message, 'contract/contract_update');
    }
}