'use strict';
const knex = require('../config/connection');
const { response } = require('../helper');
const { auth_jwt_bearer } = require('../middleware');

exports.issues_challenges_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, user_level, institution } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';
        let filtering = '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }



        let result, total = ''
        if (user_level === 'Admin') {
            if (datafilter) {
                filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
            }

            result = await knex.raw(`
                SELECT * FROM issues_challenges
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
            total = await knex.raw(`SELECT COUNT(*) AS total from issues_challenges ${filtering}`);
        }
        else {
            if (datafilter) {
                filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
            }

            result = await knex.raw(`
                SELECT * FROM issues_challenges
                WHERE institution='${institution}'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
            total = await knex.raw(`SELECT COUNT(*) AS total from issues_challenges WHERE institution='${institution}' ${filtering}`);

        }
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'issues_challenges/issues_challenges');
    }
}

exports.issues_challenges_show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('issues_challenges').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'issues_challenges/issues_challenges_show');
    }
}

exports.issues_challenges_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            institution,
            package_no,
            contract_no,
            issue_detail,
            solving_solution
        } = req.body;

        await knex('issues_challenges')
            .insert([{
                institution,
                package_no,
                contract_no,
                issue_detail,
                solving_solution,
                created_at: knex.fn.now()
            }]);
        response.ok(res, 'success insert issues_challenges');
    }
    catch (error) {
        response.error(res, error.message, 'issues_challenges/issues_challenges_insert');
    }
}

exports.issues_challenges_update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            institution,
            package_no,
            contract_no,
            issue_detail,
            solving_solution
        } = req.body;

        await knex('issues_challenges')
            .update({
                institution,
                package_no,
                contract_no,
                issue_detail,
                solving_solution,
                updated_at: knex.fn.now()
            })
            .where({ id });
        response.ok(res, 'success update issues_challenges');
    }
    catch (error) {
        response.error(res, error.message, 'issues_challenges/issues_challenges_update');
    }
}

exports.issues_challenges_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('issues_challenges').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'issues_challenges/issues_challenges_destroy');
    }
}