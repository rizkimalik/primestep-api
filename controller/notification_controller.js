'use strict';
const knex = require('../config/connection');
const { response } = require('../helper');
const { auth_jwt_bearer } = require('../middleware');

exports.notification_proposal = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { user } = req.body;
        
        let result = '';
        if (user.user_level === 'Evaluator') {
            result = await knex.raw(`
                SELECT id, type, piu, proposal_name, updated_at FROM innovation_projects
                WHERE evaluator='${user.username}' AND recommend IS NULL
                ORDER BY id DESC
            `);
        }
        else if (user.user_level === 'PIU') {
            result = await knex.raw(`
                SELECT id, type, piu, proposal_name, updated_at FROM innovation_projects
                WHERE piu='${user.institution}' AND recommend = '2'
                ORDER BY id DESC
            `); 
        }
        
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'notification/notification_proposal');
    }
}


exports.notification_document = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { user } = req.body;
        
        let result = '';
        if (user.user_level === 'PIU') {
            result = await knex.raw(`
                SELECT * FROM document_upload
                WHERE piu='${user.institution}' AND approved = '0'
                ORDER BY id DESC
            `); 
        }
        
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'notification/notification_document');
    }
}

/* 
SELECT a.*, b.piu
FROM document_upload a 
LEFT JOIN innovation_projects b ON b.proposal_no=a.package_no
WHERE b.piu = 'UI' AND a.approved = '0' 
-- ORDER BY a.id DESC
UNION ALL
SELECT a.*, b.institution
FROM document_upload a 
LEFT JOIN strengthen_capability b ON b.proposal_no=a.package_no
WHERE b.institution = 'UI' AND a.approved = '0' 
-- ORDER BY a.id DESC
UNION ALLh
SELECT a.*, b.institution_code
FROM document_upload a 
LEFT JOIN package_list b ON b.package_no=a.package_no
WHERE b.institution_code = 'UI' AND a.approved = '0' 
-- ORDER BY a.id DESC; 
*/