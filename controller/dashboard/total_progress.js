'use strict';
const knex = require('../../config/connection');
const { auth_jwt_bearer } = require('../../middleware');
const response = require('../../helper/json_response');

exports.total_disbursed_and_packages = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { auth, get_year } = req.body;

        let total = '';
        if (auth.user_level === 'PIU') {
            total = await knex.raw(`
                SELECT (SELECT SUM(disbursement_value) FROM disbursement_fund WHERE YEAR(disbursement_date)='${get_year}' AND institution='${auth.institution}') AS total_funds_disbursement,
                (SELECT COUNT(*) FROM package_list WHERE deleted_at is null AND advertisment_year='${get_year}' AND institution_code='${auth.institution}') AS total_procurement_packages,
                (SELECT COUNT(*) FROM innovation_projects WHERE deleted_at is null AND proposed_year='${get_year}' AND piu='${auth.institution}') AS total_research_startup
            `);
        } 
        else {
            total = await knex.raw(`
                SELECT (SELECT SUM(disbursement_value) FROM disbursement_fund WHERE YEAR(disbursement_date)='${get_year}') AS total_funds_disbursement,
                (SELECT COUNT(*) FROM package_list WHERE deleted_at is null AND advertisment_year='${get_year}') AS total_procurement_packages,
                (SELECT COUNT(*) FROM innovation_projects WHERE deleted_at is null AND proposed_year='${get_year}') AS total_research_startup
            `);
        }
        
        response.ok(res, total[0][0]);
    }
    catch (error) {
        response.error(res, error.message, 'dashboard/total_disbursed_and_packages');
    }
}

exports.total_procurement_progress = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { get_year, institution } = req.body;
        const total = await knex.raw(`
            SELECT 
                SUM(CASE WHEN status IS NULL THEN 1 ELSE 0 END) AS total_paket_sisa,
                SUM(CASE WHEN status IS NOT NULL AND status != 'Selesai' THEN 1 ELSE 0 END) AS total_paket_aktif,
                SUM(CASE WHEN status = 'Selesai' THEN 1 ELSE 0 END) AS total_paket_selesai,
                COUNT(*) as total_paket
            FROM package_list 
            WHERE deleted_at is null AND advertisment_year = '${get_year}' AND institution_code = '${institution}'
        `);
        response.ok(res, total[0][0]);
    }
    catch (error) {
        response.error(res, error.message, 'dashboard/total_procurement_progress');
    }
}
