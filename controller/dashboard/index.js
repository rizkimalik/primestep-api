const total_progress = require('./total_progress');
const loan_milestone = require('./loan_milestone');
const dash_evaluator = require('./dash_evaluator');

module.exports = {
    total_progress,
    loan_milestone,
    dash_evaluator,
}