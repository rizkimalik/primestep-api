'use strict';
const knex = require('../../config/connection');
const { auth_jwt_bearer } = require('../../middleware');
const response = require('../../helper/json_response');

exports.loan_milestone = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { auth } = req.body;

        const result = await knex('loan_milestone').first();
        if (auth.user_level === 'PIU') {
            const { total_withdrawal } = await knex('loan_contract').sum('contract_value AS total_withdrawal').where({ institution: auth.institution }).first();
            const { total_disbursement } = await knex('disbursement_fund').sum('disbursement_value AS total_disbursement').where({ institution: auth.institution }).first();
            const { alocation_plan } = await knex('package_alocation').select('alocation_plan').where({ code: auth.institution }).first();
            result.total_withdrawal = total_withdrawal;
            result.total_disbursement = total_disbursement;
            result.alocation_plan = alocation_plan;
        }
        else {
            const { total_withdrawal } = await knex('loan_contract').sum('contract_value AS total_withdrawal').first();
            const { total_disbursement } = await knex('disbursement_fund').sum('disbursement_value AS total_disbursement').first();
            result.total_withdrawal = total_withdrawal;
            result.total_disbursement = total_disbursement;
        }

        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'dashboard/loan_milestone_amount');
    }
}

exports.loan_progress_permonth = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { auth, get_year } = req.body;
        auth_jwt_bearer(req, res);
        const result = {};
        let where_condition_withdrawal, where_condition_disbursement = '';
        if (auth.user_level === 'PIU') {
            where_condition_withdrawal = `WHERE YEAR(contract_date)='${get_year}' AND institution='${auth.institution}'`;
            where_condition_disbursement = `WHERE YEAR(disbursement_date)='${get_year}' AND institution='${auth.institution}'`;
        } else {
            where_condition_withdrawal = `WHERE YEAR(contract_date)='${get_year}'`;
            where_condition_disbursement = `WHERE YEAR(disbursement_date)='${get_year}'`;
        }
        const loan_withdrawal = await knex.raw(`
            SELECT COALESCE(SUM(contract_value_sum), 0) AS total_withdrawal, months.month_number
            FROM (
                SELECT 1 AS month_number UNION ALL
                SELECT 2 UNION ALL
                SELECT 3 UNION ALL
                SELECT 4 UNION ALL
                SELECT 5 UNION ALL
                SELECT 6 UNION ALL
                SELECT 7 UNION ALL
                SELECT 8 UNION ALL
                SELECT 9 UNION ALL
                SELECT 10 UNION ALL
                SELECT 11 UNION ALL
                SELECT 12
            ) AS months
            LEFT JOIN (
                SELECT SUM(contract_value) AS contract_value_sum, MONTH(contract_date) AS month_number
                FROM loan_contract
                ${where_condition_withdrawal}
                GROUP BY MONTH(contract_date)
            ) AS data ON months.month_number = data.month_number
            GROUP BY months.month_number
        `);

        const loan_disbursement = await knex.raw(`
            SELECT COALESCE(SUM(disbursement_value_sum), 0) AS total_disbursment, months.month_number
            FROM (
                SELECT 1 AS month_number UNION ALL
                SELECT 2 UNION ALL
                SELECT 3 UNION ALL
                SELECT 4 UNION ALL
                SELECT 5 UNION ALL
                SELECT 6 UNION ALL
                SELECT 7 UNION ALL
                SELECT 8 UNION ALL
                SELECT 9 UNION ALL
                SELECT 10 UNION ALL
                SELECT 11 UNION ALL
                SELECT 12
            ) AS months
            LEFT JOIN (
                SELECT SUM(disbursement_value) AS disbursement_value_sum, MONTH(disbursement_date) AS month_number
                FROM disbursement_fund
                ${where_condition_disbursement}
                GROUP BY MONTH(disbursement_date)
            ) AS data ON months.month_number = data.month_number
            GROUP BY months.month_number
        `);

        result.loan_withdrawal = loan_withdrawal[0];
        result.loan_disbursement = loan_disbursement[0];
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'dashboard/loan_progress_permonth');
    }
}
