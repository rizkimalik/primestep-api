'use strict';
const knex = require('../../config/connection');
const response = require('../../helper/json_response');
const { auth_jwt_bearer } = require('../../middleware');

exports.total_packages = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { username, get_year } = req.body;

        const total = await knex.raw(`
            SELECT 
                SUM(CASE WHEN recommend = '0' THEN 1 ELSE 0 END) AS total_revisi,
                SUM(CASE WHEN recommend = '1' THEN 1 ELSE 0 END) AS total_rekomendasi,
                SUM(CASE WHEN recommend = '2' THEN 1 ELSE 0 END) AS total_ditolak,
                SUM(CASE WHEN recommend IS NULL THEN 1 ELSE 0 END) AS total_menunggu
            FROM innovation_projects
            WHERE deleted_at IS NULL AND evaluator = '${username}' AND proposed_year='${get_year}'
        `);
        response.ok(res, total[0][0]);
    }
    catch (error) {
        response.error(res, error.message, 'dashboard/total_packages');
    }
}

exports.data_packages = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, username, get_year, status } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY a.id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let result, total = '';
        if (status === 'ditolak') {
            result = await knex.raw(`
                SELECT a.*, c.focus_name, d.framework
                FROM innovation_projects AS a
                LEFT JOIN category_focus AS c ON a.category_focus = c.id
                LEFT JOIN category_framework AS d ON a.category_framework = d.id
                WHERE a.deleted_at is null AND a.evaluator='${username}' AND a.proposed_year='${get_year}' AND a.recommend = '2'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
            total = await knex.raw(`
                SELECT COUNT(*) AS total FROM innovation_projects AS a
                LEFT JOIN category_focus AS c ON a.category_focus = c.id
                LEFT JOIN category_framework AS d ON a.category_framework = d.id
                WHERE a.deleted_at is null AND a.evaluator='${username}' AND a.proposed_year='${get_year}' AND a.recommend = '2'
                ${filtering}
            `);
        }
        else if (status === 'rekomendasi') {
            result = await knex.raw(`
                SELECT a.*, c.focus_name, d.framework
                FROM innovation_projects AS a
                LEFT JOIN category_focus AS c ON a.category_focus = c.id
                LEFT JOIN category_framework AS d ON a.category_framework = d.id
                WHERE a.deleted_at is null AND a.evaluator='${username}' AND a.proposed_year='${get_year}' AND a.recommend = '1'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
            total = await knex.raw(`
                SELECT COUNT(*) AS total FROM innovation_projects AS a
                LEFT JOIN category_focus AS c ON a.category_focus = c.id
                LEFT JOIN category_framework AS d ON a.category_framework = d.id
                WHERE a.deleted_at is null AND a.evaluator='${username}' AND a.proposed_year='${get_year}' AND a.recommend = '1'
                ${filtering}
            `);
        } 
        else if (status === 'revisi') {
            result = await knex.raw(`
                SELECT a.*, c.focus_name, d.framework
                FROM innovation_projects AS a
                LEFT JOIN category_focus AS c ON a.category_focus = c.id
                LEFT JOIN category_framework AS d ON a.category_framework = d.id
                WHERE a.deleted_at is null AND a.evaluator='${username}' AND a.proposed_year='${get_year}' AND a.recommend = '0'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
            total = await knex.raw(`
                SELECT COUNT(*) AS total FROM innovation_projects AS a
                LEFT JOIN category_focus AS c ON a.category_focus = c.id
                LEFT JOIN category_framework AS d ON a.category_framework = d.id
                WHERE a.deleted_at is null AND a.evaluator='${username}' AND a.proposed_year='${get_year}' AND a.recommend = '0'
                ${filtering}
            `);
        }
        else if (status === 'menunggu') {
            result = await knex.raw(`
                SELECT a.*, c.focus_name, d.framework
                FROM innovation_projects AS a
                LEFT JOIN category_focus AS c ON a.category_focus = c.id
                LEFT JOIN category_framework AS d ON a.category_framework = d.id
                WHERE a.deleted_at is null AND a.evaluator='${username}' AND a.proposed_year='${get_year}' AND a.recommend is null
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
            total = await knex.raw(`
                SELECT COUNT(*) AS total FROM innovation_projects AS a
                LEFT JOIN category_focus AS c ON a.category_focus = c.id
                LEFT JOIN category_framework AS d ON a.category_framework = d.id
                WHERE a.deleted_at is null AND a.evaluator='${username}' AND a.proposed_year='${get_year}' AND a.recommend is null
                ${filtering}
            `);
        }

        const result_data = result[0];
        for (let i = 0; i < result_data.length; i++) {
            if (result_data[i].package_no) {
                const proposal_document = await knex('document_upload').where({ package_no: result_data[i].package_no, document_for: 'judul-proposal' }).first();
                result_data[i].proposal_document = proposal_document;
            }
            else {
                result_data[i].proposal_document = '';
            }
        }
        
        response.data(res, result[0], total[0][0].total);

    }
    catch (error) {
        response.error(res, error.message, 'dashboard/data_packages');
    }
}