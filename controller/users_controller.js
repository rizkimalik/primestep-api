'use strict';
const knex = require('../config/connection');
const bcrypt = require('bcryptjs');
const { auth_jwt_bearer } = require('../middleware');
const logger = require('../helper/logger');
const response = require('../helper/json_response');

const index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY user_level,username ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM users
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from users ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error, 'dashboard/data_package');
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const getUser = await knex('users').where('id', id);
        response.ok(res, getUser);
    }
    catch (error) {
        logger('user/show', error);
        res.status(500).end();
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            name,
            username,
            email_address,
            password,
            user_level,
            institution
        } = req.body;

        const salt = bcrypt.genSaltSync(10)
        const passwordHash = bcrypt.hashSync(password, salt)

        await knex('users')
            .insert([{
                name,
                username,
                email_address,
                password: passwordHash,
                user_level,
                institution,
                login: 0,
                created_at: knex.fn.now()
            }]);

        const getUser = await knex('users').where({ username }).first();
        response.ok(res, getUser);
    }
    catch (error) {
        logger('user/store', error);
        res.status(500).end();
    }
}


const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);

        const {
            id,
            name,
            username,
            email_address,
            user_level,
            institution
        } = req.body;

        await knex('users')
            .where({ id })
            .update({
                name,
                username,
                email_address,
                user_level,
                institution,
                updated_at: knex.fn.now()
            });
        const getData = await knex('users').where({ id }).first();
        response.ok(res, getData);
    }
    catch (error) {
        logger('user/update', error);
        res.status(500).end();
    }
}


const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);

        const { id } = req.params;
        const delData = await knex('users').where({ id }).del();
        response.ok(res, delData);
    }
    catch (error) {
        logger('user/destroy', error);
        res.status(500).end();
    }
}

const reset_password = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id, password } = req.body;
        const salt = bcrypt.genSaltSync(10)
        const passwordHash = bcrypt.hashSync(password, salt)

        await knex('users')
            .where({ id })
            .update({ password: passwordHash });
        response.ok(res, 'success reset password');
    }
    catch (error) {
        logger('user/reset_password', error);
        res.status(500).end();
    }
}

module.exports = {
    index,
    show,
    store,
    update,
    destroy,
    reset_password
}
