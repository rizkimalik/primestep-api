'use strict';
const knex = require('../../config/connection');
const { response, currency_to_number } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

exports.strengthen_pnbp_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, institution, adv_year } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM (
                    SELECT a.*, b.category_name FROM strengthen_pnbp a
                    LEFT JOIN strengthen_category b ON a.category_id = b.id
                ) as strengthen_data
                WHERE deleted_at is null AND institution='${institution}' AND adv_year='${adv_year}'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total from (
                SELECT a.*, b.category_name FROM strengthen_pnbp a
                LEFT JOIN strengthen_category b ON a.category_id = b.id
            ) as strengthen_data
            WHERE deleted_at is null AND institution='${institution}' AND adv_year='${adv_year}' ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error, 'strengthen_pnbp/strengthen_pnbp_list');
    }
}

exports.information_header_status = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { institution, adv_year } = req.body;

        const where_condition = `WHERE deleted_at is null AND institution='${institution}' AND adv_year='${adv_year}'`;
        const result = await knex.raw(`
            SELECT SUM(est_value_idr) as sum_value, COUNT(*) as count_value
            FROM strengthen_pnbp ${where_condition}
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_pnbp/information_header_status');
    }
}

exports.strengthen_pnbp_show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('strengthen_pnbp').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_pnbp/strengthen_pnbp_show');
    }
}

exports.strengthen_pnbp_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            proposal_no,
            package_no,
            institution,
            bentuk_program,
            est_value_idr,
            indicator_id,
            category_id,
            adv_year,
            quarter,
            username,
        } = req.body;

        await knex('strengthen_pnbp')
            .insert([{
                proposal_no,
                package_no,
                institution,
                indicator_id,
                category_id,
                bentuk_program,
                adv_year,
                quarter,
                status: 'Usulan',
                est_value_idr: currency_to_number(est_value_idr),
                created_by: username,
                created_at: knex.fn.now()
            }]);
        response.ok(res, 'success strengthen_pnbp_insert');
    }
    catch (error) {
        response.error(res, error, 'strengthen_pnbp/strengthen_pnbp_insert');
    }
}

exports.strengthen_pnbp_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            proposal_no,
            package_no,
            institution,
            bentuk_program,
            est_value_idr,
            indicator_id,
            category_id,
            adv_year,
            quarter,
            username,
        } = req.body;

        await knex('strengthen_pnbp')
            .update({
                proposal_no,
                package_no,
                institution,
                bentuk_program,
                category_id,
                indicator_id,
                adv_year,
                quarter,
                est_value_idr: currency_to_number(est_value_idr),
                updated_by: username,
                updated_at: knex.fn.now()
            })
            .where({ id });
        response.ok(res, 'success strengthen_pnbp_update');
    }
    catch (error) {
        response.error(res, error, 'strengthen_pnbp/strengthen_pnbp_update');
    }
}

exports.strengthen_pnbp_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('strengthen_pnbp').update({ deleted_at: knex.fn.now() }).where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'strengthen_pnbp/strengthen_pnbp_destroy');
    }
}

exports.dmf_indicator_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const result = await knex.raw(`
            SELECT * FROM dmf_indicator order by id asc
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_pnbp/dmf_indicator_list');
    }
}


/**
 *  Export Data modul
 */
exports.export_strengthen_pnbp = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { institution, adv_year } = req.body;

        let where_condition = `WHERE deleted_at is null AND institution='${institution}' AND adv_year='${adv_year}'`;
        const result = await knex.raw(`
            SELECT * FROM (
                SELECT a.*, b.category_name FROM strengthen_pnbp a
                LEFT JOIN strengthen_category b ON a.category_id = b.id
            ) as strengthen_data
            ${where_condition}
        `);

        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_pnbp/export_strengthen_pnbp');
    }
}
