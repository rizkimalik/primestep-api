'use strict';
const knex = require('../../config/connection');
const { response, currency_to_number } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

//? component 3
exports.strengthen_capability_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, institution, adv_year } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM (
                    SELECT a.*, b.category_name FROM strengthen_capability a
                    LEFT JOIN strengthen_category b ON a.category_id = b.id
                ) as strengthen_data
                WHERE deleted_at is null AND institution='${institution}' AND adv_year='${adv_year}'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total from (
                SELECT a.*, b.category_name FROM strengthen_capability a
                LEFT JOIN strengthen_category b ON a.category_id = b.id
            ) as strengthen_data
            WHERE deleted_at is null AND institution='${institution}' AND adv_year='${adv_year}' ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error, 'strengthen_capability/strengthen_capability_list');
    }
}

exports.information_header_status = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { institution, adv_year } = req.body;

        const where_condition = `WHERE deleted_at is null AND institution='${institution}' AND adv_year='${adv_year}'`;
        // const result = await knex.raw(`
        //     SELECT SUM(est_value_idr) as sum_value, COUNT(*) as count_value, SUBSTRING_INDEX(status,'-',1) AS status
        //     FROM strengthen_capability ${where_condition}
        // `);
        const result = await knex.raw(`
            SELECT SUM(est_value_idr) as sum_value, COUNT(*) as count_value, status
            FROM strengthen_capability ${where_condition}
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/information_header_status');
    }
}

exports.strengthen_capability_show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('strengthen_capability').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/strengthen_capability_show');
    }
}

exports.strengthen_capability_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            proposal_no,
            package_no,
            institution,
            component,
            strengthen,
            est_value_idr,
            indicator_id,
            category_id,
            adv_year,
            quarter,
            unit,
            volume,
            username,
        } = req.body;

        await knex('strengthen_capability')
            .insert([{
                proposal_no,
                package_no,
                institution,
                indicator_id,
                category_id,
                component,
                strengthen,
                adv_year,
                quarter,
                volume,
                unit,
                status: 'Usulan',
                est_value_idr: currency_to_number(est_value_idr),
                created_by: username,
                created_at: knex.fn.now()
            }]);
        response.ok(res, 'success strengthen_capability_insert');
    }
    catch (error) {
        response.error(res, error, 'strengthen_capability/strengthen_capability_insert');
    }
}

exports.strengthen_capability_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            proposal_no,
            package_no,
            institution,
            component,
            strengthen,
            est_value_idr,
            indicator_id,
            category_id,
            adv_year,
            quarter,
            unit,
            volume,
            username,
        } = req.body;

        await knex('strengthen_capability')
            .update({
                proposal_no,
                package_no,
                institution,
                component,
                strengthen,
                category_id,
                indicator_id,
                adv_year,
                quarter,
                volume,
                unit,
                est_value_idr: currency_to_number(est_value_idr),
                updated_by: username,
                updated_at: knex.fn.now()
            })
            .where({ id });
        response.ok(res, 'success strengthen_capability_update');
    }
    catch (error) {
        response.error(res, error, 'strengthen_capability/strengthen_capability_update');
    }
}

exports.strengthen_capability_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('strengthen_capability').update({ deleted_at: knex.fn.now() }).where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'strengthen_capability/strengthen_capability_destroy');
    }
}


exports.contract_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { auth, adv_year } = req.body;

        let where_condition = '';
        if (auth.user_level === 'PIU') {
            where_condition = `WHERE deleted_at is null AND a.status NOT IN ('Usulan','Persiapan-Dokumen') AND a.institution='${auth.institution}' AND a.adv_year='${adv_year}'`;
        }
        else {
            where_condition = `WHERE deleted_at is null AND a.status NOT IN ('Usulan','Persiapan-Dokumen') AND a.adv_year='${adv_year}'`;
        }

        const result = await knex.raw(`
            SELECT a.institution, a.proposal_no, a.adv_year, a.contract_no, b.institution_name, a.status  
            FROM strengthen_capability a 
            LEFT JOIN institution b on a.institution = b.institution_code
            ${where_condition}
            GROUP BY a.institution, a.proposal_no, a.adv_year, a.contract_no, b.institution_name, a.status
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/contract_list');
    }
}

exports.strengthen_capability_verified = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, institution, adv_year } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 5;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM strengthen_capability
            WHERE deleted_at is null AND status NOT IN ('Usulan','Persiapan-Dokumen') AND institution='${institution}' AND adv_year='${adv_year}'
            ${orderby}
        `);
        response.ok(res, result[0]);

        // ${filtering}
        // ${orderby}
        // LIMIT ${datatake} OFFSET ${dataskip}

        // const total = await knex.raw(`
        //     SELECT COUNT(*) AS total from strengthen_capability 
        //     WHERE deleted_at is null AND status NOT IN ('Usulan','Persiapan-Dokumen') AND institution='${institution}' AND adv_year='${adv_year}'
        // `);
        // response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error, 'strengthen_capability/strengthen_capability_verified');
    }
}


exports.dmf_indicator_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const result = await knex.raw(`
            SELECT * FROM dmf_indicator order by id asc
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/dmf_indicator_list');
    }
}


exports.sum_disbursement_perquarter = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { institution, year, quarter } = req.body;

        const where_condition = `WHERE deleted_at is null AND institution='${institution}' AND adv_year='${year}' AND quarter='${quarter}'`;
        const result = await knex.raw(`
            SELECT SUM(est_value_idr) as sum_value_perquarter
            FROM strengthen_capability 
            ${where_condition}
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/sum_disbursement_perquarter');
    }
}


/**
 *  Export Data modul
 */
exports.export_strengthen_capability = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { institution, adv_year } = req.body;

        let where_condition = `WHERE deleted_at is null AND institution='${institution}' AND adv_year='${adv_year}'`;
        const result = await knex.raw(`
            SELECT * FROM (
                SELECT a.*, b.category_name FROM strengthen_capability a
                LEFT JOIN strengthen_category b ON a.category_id = b.id
            ) as strengthen_data
            ${where_condition}
        `);

        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/export_strengthen_capability');
    }
}
