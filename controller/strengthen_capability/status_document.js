'use strict';
const knex = require('../../config/connection');
const { response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

//? tahap progress (proposal_no) => persiapan, verifikasi, kontrak, withdrawal app, pencairan, finish
exports.update_status_progress = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { proposal_no, status } = req.body;

        if (proposal_no) {
            if (status === 'Persiapan-Dokumen') {
                const usulan_list = await knex('strengthen_capability').where({ proposal_no, status: null, contract_no: null }).orWhere({ proposal_no, status: 'Usulan', contract_no: null });
                for (let i = 0; i < usulan_list.length; i++) {
                    await knex('strengthen_capability').update({ status, updated_at: knex.fn.now() }).where({ package_no: usulan_list[i].package_no });
                }
            }
            else if (status === 'Verifikasi-Dokumen') {
                const persiapan_list = await knex('strengthen_capability').where({ proposal_no, status: 'Persiapan-Dokumen', contract_no: null });
                for (let i = 0; i < persiapan_list.length; i++) {
                    await knex('strengthen_capability').update({ status, updated_at: knex.fn.now() }).where({ package_no: persiapan_list[i].package_no });
                }
            }
            else if (status === 'Kontrak') {
                const data_list = await knex('strengthen_capability').where({ proposal_no, status: 'Verifikasi-Dokumen' });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('strengthen_capability').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }
            // else if (status === 'Withdrawal-App') {
            //     const data_list = await knex('strengthen_capability').where({ proposal_no, status: 'Kontrak' });
            //     for (let i = 0; i < data_list.length; i++) {
            //         await knex('strengthen_capability').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
            //     }
            // }
            // else if (status === 'Pencairan') {
            //     const data_list = await knex('strengthen_capability').where({ proposal_no, status: 'Kontrak' });
            //     for (let i = 0; i < data_list.length; i++) {
            //         await knex('strengthen_capability').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
            //     }
            // }

            else if (status === 'Pencairan-Q1') {
                const data_list = await knex('strengthen_capability').where({ proposal_no, status: 'Kontrak' });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('strengthen_capability').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }
            else if (status === 'Pencairan-Q2') {
                const data_list = await knex('strengthen_capability').where({ proposal_no, status: 'Pencairan-Q1' });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('strengthen_capability').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }
            else if (status === 'Pencairan-Q3') {
                const data_list = await knex('strengthen_capability').where({ proposal_no, status: 'Pencairan-Q2' });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('strengthen_capability').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }
            else if (status === 'Pencairan-Q4') {
                const data_list = await knex('strengthen_capability').where({ proposal_no, status: 'Pencairan-Q3' });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('strengthen_capability').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }

            else if (status === 'Finish') {
                const data_list = await knex('strengthen_capability').where({ proposal_no, status: 'Pencairan-Q4' });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('strengthen_capability').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }

            response.ok(res, 'success update_status_progress');
        }
        else {
            response.error(res, 'package number failed', 'strengthen_capability/update_status_progress');
        }
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/update_status_progress');
    }
}

//? document status
exports.updated_status_verifikasi_document = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            id,
            approved,
            approved_comment,
            approved_evaluator,
        } = req.body;

        await knex('document_upload')
            .update({
                approved,
                approved_comment,
                approved_evaluator,
                updated_at: knex.fn.now()
            })
            .where({ id });

        response.ok(res, 'success updated_status_verifikasi_document');
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/updated_status_verifikasi_document');
    }
}

exports.updated_status_revisi_document = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.body;
        await knex('document_upload').update({ status: 'Persiapan-Dokumen-Revisi' }).where({ id });
        response.ok(res, 'success updated_status_revisi_document');
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/updated_status_revisi_document');
    }
}

exports.history_verifikasi_document = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no, document_for, document_name } = req.body;
        const result = await knex.raw(`
            SELECT * FROM document_upload WHERE package_no='${package_no}' AND document_for='${document_for}' AND document_name='${document_name}' ORDER BY id DESC
        `);
        const data = result[0];
        for (let i = 0; i < data.length; i++) {
            const user = await knex('users').select('name').where({ username: data[i].approved_evaluator }).first();
            data[i].approved_name = user?.name;
        }
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/history_verifikasi_document');
    }
}

exports.updated_status_contract = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            proposal_no,
            contract_no,
        } = req.body;
        await knex('strengthen_capability').update({ contract_no }).where({ proposal_no, status: 'Verifikasi-Dokumen' });
        response.ok(res, 'success updated_status_contract');
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/updated_status_contract');
    }
}

exports.updated_status_withdrawal = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            proposal_no,
            status,
        } = req.body;
        await knex('strengthen_capability').update({ status }).where({ proposal_no });
        response.ok(res, 'success updated_status_withdrawal');
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/updated_status_withdrawal');
    }
}


exports.contract_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no } = req.body;
        const result = await knex('loan_contract').where({ package_no }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/contract_detail');
    }
}

exports.package_status_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no } = req.body;
        const result = await knex('package_status').where({ package_no });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/package_status_detail');
    }
}

exports.check_verification_document = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { package_no, document_for, status } = req.body;

        const { count } = await knex.count('id as count').from('document_upload')
            .where({ package_no, document_for, status, approved: '0' })
            .orWhere({ package_no, document_for, status, approved: null }).first();

        response.ok(res, count);
    }
    catch (error) {
        response.error(res, error.message, 'strengthen_capability/check_verification_document');
    }
}