const strengthen_capability = require('./strengthen_capability');
const strengthen_pnbp = require('./strengthen_pnbp');
const status_document = require('./status_document');

module.exports =  {
    strengthen_capability,
    strengthen_pnbp,
    status_document,
}