'use strict';
const knex = require('../../config/connection');
const { response, logger } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

//? innvovation leaders
exports.innovation_leader_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no } = req.body;
        const result = await knex('innovation_leaders').where({ package_no }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'innovation_leader/innovation_leaders_detail');
    }
}

exports.innovation_leader_insert = async function (data) {
    try {
        const {
            package_no,
            leader_name,
            title_position,
            identity_id,
            gender,
            email,
            phone_number,
            address,
            graduation,
        } = data;

        await knex('innovation_leaders')
            .insert([{
                package_no,
                leader_name,
                title_position,
                identity_id,
                gender,
                email,
                phone_number,
                address,
                graduation,
            }]);
        return 'success innovation_leader_insert';
    }
    catch (error) {
        logger('innovation_leader/innovation_leader_insert', error.message)
    }
}

exports.innovation_leader_update = async function (data) {
    try {
        const {
            package_no,
            leader_name,
            title_position,
            identity_id,
            gender,
            email,
            phone_number,
            address,
            graduation,
        } = data;

        const check = await knex('innovation_leaders').where({ package_no });
        if (check.length > 0) {
            await knex('innovation_leaders')
                .update({
                    leader_name,
                    title_position,
                    identity_id,
                    gender,
                    email,
                    phone_number,
                    address,
                    graduation,
                })
                .where({ package_no });
        } else {
            await knex('innovation_leaders')
                .insert([{
                    package_no,
                    leader_name,
                    title_position,
                    identity_id,
                    gender,
                    email,
                    phone_number,
                    address,
                    graduation,
                }]);
        }

        return 'success innovation_leader_update';
    }
    catch (error) {
        logger('innovation_leader/innovation_leader_update', error.message)
    }
}
