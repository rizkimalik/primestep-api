'use strict';
const knex = require('../../config/connection');
const { response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');


//? hisotry TKT
exports.history_tkt_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { data_fields } = req.body;

        for (let i = 0; i < data_fields.length; i++) {
            await knex('history_tkt').where({ institution: data_fields[i].institution, package_no: data_fields[i].package_no, indicator_id: data_fields[i].indicator_id }).del()
        }

        if (data_fields.length > 0) {
            await knex('history_tkt').insert(data_fields);
            response.ok(res, 'success history_tkt_insert');
        }
        else {
            response.error(res, 'data fields kosong.', 'startup_research/history_tkt_insert');
        }
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/history_tkt_insert');
    }
}

exports.history_tkt_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { package_no, category_tkt } = req.body;
        const result = await knex('history_tkt').where({ package_no, category_tkt });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/history_tkt_list');
    }
}

