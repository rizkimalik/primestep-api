const knex = require("../../config/connection");
const bcrypt = require('bcryptjs');
const { response } = require("../../helper");
const { auth_jwt_bearer } = require("../../middleware");

exports.evaluator_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY username ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM users
                WHERE user_level='Evaluator'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from users WHERE user_level='Evaluator' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'evaluator/evaluator_list');
    }
}

exports.evaluator_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.body;
        const getUser = await knex('users').where({ id }).first();
        response.ok(res, getUser);
    }
    catch (error) {
        response.error(res, error.message, 'evaluator/evaluator_detail');
    }
}

exports.evaluator_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            name,
            username,
            password,
            email_address,
            institution_name,
            focus_area,
        } = req.body;

        const salt = bcrypt.genSaltSync(10)
        const passwordHash = bcrypt.hashSync(password, salt)

        await knex('users')
            .insert({
                name,
                username,
                email_address,
                password: passwordHash,
                user_level: 'Evaluator',
                institution: 'PMU',
                institution_name,
                focus_area,
                login: 0,
                created_at: knex.fn.now()
            });

        const getUser = await knex('users').where({ username }).first();
        response.ok(res, getUser);
    }
    catch (error) {
        response.error(res, error.message, 'evaluator/evaluator_insert');
    }
}


exports.evaluator_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            name,
            username,
            password,
            email_address,
            institution_name,
            focus_area,
        } = req.body;

        const salt = bcrypt.genSaltSync(10)
        const passwordHash = bcrypt.hashSync(password, salt)

        await knex('users')
            .update({
                name,
                username,
                password: passwordHash,
                email_address,
                institution_name,
                focus_area,
                updated_at: knex.fn.now()
            })
            .where({ id });

        const getUser = await knex('users').where({ username }).first();
        response.ok(res, getUser);
    }
    catch (error) {
        response.error(res, error.message, 'evaluator/evaluator_update');
    }
}

exports.evaluator_delete = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const delData = await knex('users').where({ id }).del();
        response.ok(res, delData);
    }
    catch (error) {
        response.error(res, error.message, 'evaluator/evaluator_delete');
    }
}

exports.set_assign_evaluator = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            package_no,
            evaluator,
        } = req.body;

        await knex('innovation_projects')
            .update({
                evaluator,
                updated_at: knex.fn.now()
            })
            .where({ package_no });
        response.ok(res, 'success set_assign_evaluator');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/set_assign_evaluator');
    }
}