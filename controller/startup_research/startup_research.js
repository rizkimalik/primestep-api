'use strict';
const knex = require('../../config/connection');
const { response, currency_to_number, logger } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');
const { innovation_leader_insert, innovation_leader_update } = require('./innovation_leader');

//? component 2 - startup
exports.startup_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, auth, piu, proposed_year } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY a.recommend ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let where_condition = '';
        if (auth.user_level === 'PIU') {
            where_condition = `WHERE a.deleted_at is null AND a.type='S' AND (jenis_usulan='0' OR jenis_usulan is null) AND a.piu='${piu}' AND a.proposed_year='${proposed_year}'`;
        }
        else if (auth.user_level === 'Evaluator') {
            where_condition = `WHERE a.deleted_at is null AND a.type='S' AND (jenis_usulan='0' OR jenis_usulan is null) AND a.piu='${piu}' AND a.proposed_year='${proposed_year}' AND evaluator='${auth.username}' AND recommend IS NULL`;
        }
        else {
            where_condition = `WHERE a.deleted_at is null AND a.type='S' AND (jenis_usulan='0' OR jenis_usulan is null) AND a.piu='${piu}' AND a.proposed_year='${proposed_year}'`;
        }

        const result = await knex.raw(`
            SELECT a.*, b.contract_value, b.contract_vendor, c.focus_name, d.framework, e.name AS evaluator_name
            FROM innovation_projects AS a
            LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            LEFT JOIN users AS e ON a.evaluator = e.username
            ${where_condition}
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total FROM innovation_projects AS a
            LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            LEFT JOIN users AS e ON a.evaluator = e.username
            ${where_condition} 
            ${filtering}
        `);

        const result_data = result[0];
        for (let i = 0; i < result_data.length; i++) {
            if (result_data[i].package_no) {
                const proposal_document = await knex('document_upload').where({ package_no: result_data[i].package_no, document_for: 'judul-proposal' }).orderBy('id', 'desc').first();
                result_data[i].proposal_document = proposal_document;
            }
            else {
                result_data[i].proposal_document = '';
            }
        }
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/startup_list');
    }
}

//? research
exports.research_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, auth, piu, proposed_year } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY a.recommend ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let where_condition = '';
        if (auth.user_level === 'PIU') {
            where_condition = `WHERE a.deleted_at is null AND a.type='R' AND a.piu='${piu}' AND a.proposed_year='${proposed_year}'`;
        }
        else if (auth.user_level === 'Evaluator') {
            where_condition = `WHERE a.deleted_at is null AND a.type='R' AND a.piu='${piu}' AND a.proposed_year='${proposed_year}' AND evaluator='${auth.username}' AND recommend IS NULL`;
        }
        else {
            where_condition = `WHERE a.deleted_at is null AND a.type='R' AND a.piu='${piu}' AND a.proposed_year='${proposed_year}'`;
        }

        const result = await knex.raw(`
            SELECT a.*, b.contract_value, b.contract_vendor, c.focus_name, d.framework, e.name AS evaluator_name
            FROM innovation_projects AS a
            LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            LEFT JOIN users AS e ON a.evaluator = e.username
            ${where_condition}
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total FROM innovation_projects AS a
            LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            LEFT JOIN users AS e ON a.evaluator = e.username
            ${where_condition} 
            ${filtering}
        `);

        const result_data = result[0];
        for (let i = 0; i < result_data.length; i++) {
            if (result_data[i].package_no) {
                const proposal_document = await knex('document_upload').where({ package_no: result_data[i].package_no, document_for: 'judul-proposal' }).orderBy('id', 'desc').first();
                result_data[i].proposal_document = proposal_document;
            }
            else {
                result_data[i].proposal_document = '';
            }
        }
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/research_list');
    }
}

exports.startup_acceleration = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, auth, piu, proposed_year } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY a.recommend ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let where_condition = '';
        if (auth.user_level === 'PIU') {
            where_condition = `WHERE a.deleted_at is null AND a.type='S' AND a.jenis_usulan='1' AND a.piu='${piu}' AND a.proposed_year='${proposed_year}'`;
        }
        else if (auth.user_level === 'Evaluator') {
            where_condition = `WHERE a.deleted_at is null AND a.type='S' AND a.jenis_usulan='1' AND a.piu='${piu}' AND a.proposed_year='${proposed_year}' AND a.evaluator='${auth.username}' AND a.recommend IS NULL`;
        }
        else {
            where_condition = `WHERE a.deleted_at is null AND a.type='S' AND a.jenis_usulan='1' AND a.piu='${piu}' AND a.proposed_year='${proposed_year}'`;
        }

        const result = await knex.raw(`
            SELECT a.*, b.contract_value, b.contract_vendor, c.focus_name, d.framework, e.name AS evaluator_name
            FROM innovation_projects AS a
            LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            LEFT JOIN users AS e ON a.evaluator = e.username
            ${where_condition}
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total FROM innovation_projects AS a
            LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            LEFT JOIN users AS e ON a.evaluator = e.username
            ${where_condition} 
            ${filtering}
        `);

        const result_data = result[0];
        for (let i = 0; i < result_data.length; i++) {
            if (result_data[i].package_no) {
                const proposal_document = await knex('document_upload').where({ package_no: result_data[i].package_no, document_for: 'judul-proposal' }).orderBy('id', 'desc').first();
                result_data[i].proposal_document = proposal_document;
            }
            else {
                result_data[i].proposal_document = '';
            }
        }
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/startup_acceleration');
    }
}

exports.startup_acceleration_select = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, piu, auth, proposed_year } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY a.recommend ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        // if (auth.user_level === 'PIU') {
        //     where_condition = `WHERE a.deleted_at is null AND a.type='S' AND a.status='Finish' AND (a.jenis_usulan is null OR a.jenis_usulan='0') AND a.piu='${piu}'`;
        // }
        // else {
        //     where_condition = `WHERE a.deleted_at is null AND a.type='S' AND a.status='Finish' AND (a.jenis_usulan is null OR a.jenis_usulan='0') AND a.piu='${piu}'`;
        // }

        let where_condition = `WHERE a.deleted_at is null AND a.type='S' AND a.status='Finish' AND (a.jenis_usulan is null OR a.jenis_usulan='0') AND a.piu='${piu}'`;

        const result = await knex.raw(`
            SELECT a.*, b.contract_value, b.contract_vendor, c.focus_name, d.framework, e.name AS evaluator_name
            FROM innovation_projects AS a
            LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            LEFT JOIN users AS e ON a.evaluator = e.username
            ${where_condition}
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total FROM innovation_projects AS a
            LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            LEFT JOIN users AS e ON a.evaluator = e.username
            ${where_condition} 
            ${filtering}
        `);

        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/startup_acceleration');
    }
}


exports.information_header_status = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { auth, modul, piu, proposed_year } = req.body;

        let where_condition = '';
        if (!modul) {
            where_condition = `WHERE deleted_at is null AND piu='${piu}' AND proposed_year='${proposed_year}'`;
        }
        else {
            let type = modul === 'startup' ? 'S' : modul === 'startup-acceleration' ? 'S' : 'R';
            if (auth.user_level === 'Evaluator') {
                where_condition = `WHERE deleted_at is null AND type='${type}' AND piu='${piu}' AND proposed_year='${proposed_year}' AND evaluator='${auth.username}' AND recommend IS NULL`;
            }
            else if (modul === 'startup-acceleration') {
                where_condition = `WHERE deleted_at is null AND jenis_usulan='1' AND type='${type}' AND piu='${piu}' AND proposed_year='${proposed_year}'`;
            }
            else {
                where_condition = `WHERE deleted_at is null AND type='${type}' AND (jenis_usulan='0' OR jenis_usulan is null) AND piu='${piu}' AND proposed_year='${proposed_year}'`;
            }
        }


        const result = await knex.raw(`
            SELECT SUM(value_idr) as sum_value, COUNT(*) as count_value, status, proposal_no
            FROM innovation_projects ${where_condition}
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/information_header_status');
    }
}

exports.startup_research_show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const result = await knex('innovation_projects').where({ id }).first();
        const category_focus = await knex('category_focus').select('focus_name').where({ id: result.category_focus }).first();
        const category_framework = await knex('category_framework').select('framework').where({ id: result.category_framework }).first();
        const innovation_leader = await knex('innovation_leaders').where({ package_no: result.package_no }).first();
        result.category_focus_name = category_focus?.focus_name;
        result.category_framework_name = category_framework?.framework;
        result.leader = innovation_leader;

        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/startup_research_show');
    }
}

exports.startup_research_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { package_no } = req.body;
        const result = await knex('innovation_projects').where({ package_no }).first();
        if (result) {
            const category_focus = await knex('category_focus').select('focus_name').where({ id: result.category_focus }).first();
            const category_framework = await knex('category_framework').select('framework').where({ id: result.category_framework }).first();
            const innovation_leader = await knex('innovation_leaders').where({ package_no }).first();
            const proposal_document = await knex('document_upload').where({ package_no, document_for: 'judul-proposal' }).orderBy('id', 'desc').first();
            result.category_focus_name = category_focus?.focus_name;
            result.category_framework_name = category_framework?.framework;
            result.proposal_document = proposal_document;
            result.leader = innovation_leader;
        }

        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/startup_research_detail');
    }
}

exports.startup_research_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            jenis_usulan, //? flag usulan 1=lanjutan, 0=baru
            package_no,
            proposal_no,
            piu,
            proposal_name,
            proposed_year,
            type,
            category_focus,
            category_framework,
            value_idr,
            type_corporation,
            base_income_lastyear,
            product_development,
            product_featured,
            product_description,
            tkt_value,
            tkt_duration,
            tkt_category,
            username,
            //? leader data
            title_position,
            identity_id,
            leader_name,
            gender,
            email,
            phone_number,
            address,
            graduation,
        } = req.body;

        //? cek data paket
        const { count } = await knex.count('id as count').from('innovation_projects').where({ package_no }).first();
        if (count > 0) return await startup_research_update_data(req, res); //? update exist data

        if (type === 'R' && jenis_usulan === '0') {
            const result = await knex.select('identity_id').from('innovation_projects').where({ identity_id, type: 'R' });
            if (result.length === 0) {
                await knex('innovation_projects')
                    .insert({
                        package_no,
                        proposal_no,
                        piu,
                        proposal_name,
                        proposed_year,
                        type,
                        category_focus,
                        category_framework,
                        identity_id,
                        leader_name,
                        value_idr: currency_to_number(value_idr),
                        type_corporation,
                        base_income_lastyear: currency_to_number(base_income_lastyear),
                        product_development,
                        product_featured,
                        product_description,
                        tkt_value,
                        tkt_duration,
                        tkt_category,
                        jenis_usulan,
                        status: 'Usulan',
                        created_by: username,
                        created_at: knex.fn.now()
                    });

                await innovation_leader_insert({
                    package_no,
                    leader_name,
                    title_position,
                    identity_id,
                    gender,
                    email,
                    phone_number,
                    address,
                    graduation,
                });

                response.ok(res, 'success startup_research_insert');
            }
            else {
                response.created(res, 'Data already exist.');
            }
        }
        else if (type === 'R' && jenis_usulan === '1') {
            await knex('innovation_projects')
                .insert({
                    package_no,
                    proposal_no,
                    piu,
                    proposal_name,
                    proposed_year,
                    type,
                    category_focus,
                    category_framework,
                    identity_id,
                    leader_name,
                    value_idr: currency_to_number(value_idr),
                    type_corporation,
                    base_income_lastyear: currency_to_number(base_income_lastyear),
                    product_development,
                    product_featured,
                    product_description,
                    tkt_value,
                    tkt_duration,
                    tkt_category,
                    jenis_usulan,
                    status: 'Usulan',
                    created_by: username,
                    created_at: knex.fn.now()
                });

            await innovation_leader_insert({
                package_no,
                leader_name,
                title_position,
                identity_id,
                gender,
                email,
                phone_number,
                address,
                graduation,
            });

            response.ok(res, 'success startup_research_insert lanjutan');
        }
        else if (type === 'S') {
            await knex('innovation_projects')
                .insert({
                    package_no,
                    proposal_no,
                    piu,
                    proposal_name,
                    proposed_year,
                    type,
                    category_focus,
                    category_framework,
                    identity_id,
                    leader_name,
                    value_idr: currency_to_number(value_idr),
                    type_corporation,
                    base_income_lastyear: currency_to_number(base_income_lastyear),
                    product_development,
                    product_featured,
                    product_description,
                    tkt_value,
                    tkt_duration,
                    tkt_category,
                    jenis_usulan,
                    status: 'Usulan',
                    created_by: username,
                    created_at: knex.fn.now()
                });

            await innovation_leader_insert({
                package_no,
                leader_name,
                title_position,
                identity_id,
                gender,
                email,
                phone_number,
                address,
                graduation,
            });

            response.ok(res, 'success startup_research_insert');
        }

    }
    catch (error) {
        response.error(res, error.message, 'startup_research/startup_insert');
    }
}

exports.startup_research_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            package_no,
            proposal_no,
            piu,
            startup_name,
            proposal_name,
            name,
            category_focus,
            value_idr,
            value_usd,
            value_jpy,
        } = req.body;

        await knex('innovation_projects')
            .update({
                package_no,
                proposal_no,
                piu,
                startup_name,
                proposal_name,
                name,
                category_focus,
                value_idr: currency_to_number(value_idr),
                value_usd: currency_to_number(value_usd),
                value_jpy: currency_to_number(value_jpy),
                updated_at: knex.fn.now()
            })
            .where({ id });
        response.ok(res, 'success startup_research_update');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/startup_research_update');
    }
}

//? non http
const startup_research_update_data = async function (req, res) {
    try {
        const {
            package_no,
            proposal_no,
            piu,
            proposal_name,
            proposed_year,
            type,
            category_focus,
            category_framework,
            value_idr,
            type_corporation,
            base_income_lastyear,
            product_development,
            product_featured,
            product_description,
            tkt_value,
            tkt_duration,
            tkt_category,
            username,
            //? leader data
            title_position,
            identity_id,
            leader_name,
            gender,
            email,
            phone_number,
            address,
            graduation,
        } = req.body;

        await knex('innovation_projects')
            .update({
                package_no,
                proposal_no,
                piu,
                proposal_name,
                proposed_year,
                type,
                category_focus,
                category_framework,
                identity_id,
                leader_name,
                value_idr: currency_to_number(value_idr),
                type_corporation,
                base_income_lastyear: currency_to_number(base_income_lastyear),
                product_development,
                product_featured,
                product_description,
                tkt_value,
                tkt_duration,
                tkt_category,
                updated_by: username,
                updated_at: knex.fn.now()
            })
            .where({ package_no });

        await innovation_leader_update({
            package_no,
            leader_name,
            title_position,
            identity_id,
            gender,
            email,
            phone_number,
            address,
            graduation,
        });
        response.ok(res, 'success startup_research_update');
    }
    catch (error) {
        logger('startup_research/startup_research_update', error.message)
    }
}

exports.startup_research_delete = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('innovation_projects').update({ deleted_at: knex.fn.now() }).where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/startup_research_delete');
    }
}

// daftar usulan lanjutan
exports.research_usulan_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, institution, search_value } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 5;

        let orderby = 'ORDER BY a.recommend ASC';
        let where_condition = '';
        where_condition = `WHERE a.type='R' AND a.status='Finish' AND a.piu='${institution}' AND (a.leader_name LIKE '%${search_value}%' OR a.identity_id LIKE '%${search_value}%')`;

        const result = await knex.raw(`
            SELECT a.*, c.focus_name, d.framework
            FROM innovation_projects AS a
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            ${where_condition}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total FROM innovation_projects AS a
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            ${where_condition} 
        `);


        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/research_usulan_list');
    }
}


//? tahap usulan (package_no) => usulan(insert api diatas), luaran (innovation_outer.js), target-pendapatan, rab, dok-pendukung, konfirmasi
exports.step_update_target = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            package_no,
            target_income,
        } = req.body;

        await knex('innovation_projects')
            .update({
                target_income: currency_to_number(target_income),
                updated_at: knex.fn.now()
            })
            .where({ package_no });
        response.ok(res, 'success step_update_target_pendapatan');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/step_update_target');
    }
}

exports.list_packages_recommended = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, auth, piu, proposed_year } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 5;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY a.id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let where_condition = '';
        if (auth.user_level === 'PIU') {
            where_condition = `WHERE a.deleted_at is null AND a.piu='${piu}' AND a.proposed_year='${proposed_year}' AND a.recommend='1'`;
        }
        else {
            where_condition = `WHERE a.deleted_at is null AND a.piu='${piu}' AND a.proposed_year='${proposed_year}' AND a.recommend='1'`;
        }

        const result = await knex.raw(`
            SELECT a.*, b.contract_value, b.contract_vendor, c.focus_name, d.framework
            FROM innovation_projects AS a
            LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            ${where_condition}
            ${filtering}
            ${orderby}
        `);
  
        const result_data = result[0];
        for (let i = 0; i < result_data.length; i++) {
            if (result_data[i].package_no) {
                const proposal_document = await knex('document_upload').where({ package_no: result_data[i].package_no, document_for: 'judul-proposal' }).first();
                result_data[i].proposal_document = proposal_document;
            }
            else {
                result_data[i].proposal_document = '';
            }
        }
        response.ok(res, result[0]);

    }
    catch (error) {
        response.error(res, error.message, 'startup_research/list_packages_recommended');
    }
}

exports.check_verification_proposal = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { institution, year, modul } = req.body;
        const type = modul === 'startup' ? 'S' : 'R';

        const { count } = await knex.count('id as count').from('innovation_projects')
            .where({ piu: institution, proposed_year: year, type: type, recommend: '0' })
            .orWhere({ piu: institution, proposed_year: year, type: type, recommend: null }).first();

        response.ok(res, count);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/check_verification_proposal');
    }
}

exports.check_verification_document = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { package_no, document_for, status } = req.body;

        const { count } = await knex.count('id as count').from('document_upload')
            .where({ package_no, document_for, status, approved: '0' })
            .orWhere({ package_no, document_for, status, approved: null }).first();

        response.ok(res, count);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/check_verification_document');
    }
}


/**
 *  Export Data modul
 */
exports.export_research_development = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { modul, piu, proposed_year } = req.body;

        let where_condition = '';
        if (modul === 'research') {
            where_condition = `WHERE a.deleted_at is null AND a.type='R' AND a.piu='${piu}' AND a.proposed_year='${proposed_year}'`;
        }
        else if (modul === 'startup-inkubasi') {
            where_condition = `WHERE a.deleted_at is null AND a.type='S' AND (a.jenis_usulan='0' OR a.jenis_usulan is null) AND a.piu='${piu}' AND a.proposed_year='${proposed_year}'`;
        }
        else if (modul === 'startup-akselerasi') {
            where_condition = `WHERE a.deleted_at is null AND a.type='S' AND a.jenis_usulan='1' AND a.piu='${piu}' AND a.proposed_year='${proposed_year}'`;
        }

        const result = await knex.raw(`
            SELECT 
            CASE
                WHEN recommend = 0 THEN "Revisi"
                WHEN recommend = 1 THEN "Rekomendasi"
                WHEN recommend = 2 THEN "Ditolak"
                ELSE "Menunggu"
            END AS status_rekomendasi,
            a.*, b.contract_value, b.contract_vendor, c.focus_name, d.framework, e.name AS evaluator_name
            FROM innovation_projects AS a
            LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            LEFT JOIN category_focus AS c ON a.category_focus = c.id
            LEFT JOIN category_framework AS d ON a.category_framework = d.id
            LEFT JOIN users AS e ON a.evaluator = e.username
            ${where_condition}
        `);

        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/export_research_development');
    }
}
