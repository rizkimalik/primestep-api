'use strict';
const knex = require('../../config/connection');
const { response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

//? innvovation members
exports.innovation_member_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, package_no } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM innovation_members
                WHERE package_no='${package_no}'
                ${filtering}
                ${orderby}
                LIMIT ${datatake} OFFSET ${dataskip}
            `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from innovation_members WHERE package_no='${package_no}' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'innovation_member/innovation_members_list');
    }
}

exports.innovation_member_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.body;
        const result = await knex('innovation_members').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'innovation_member/innovation_member_detail');
    }
}

exports.innovation_member_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            package_no,
            member_name,
            member_gender,
            member_type,
            member_identity,
            member_roles,
            member_phone,
            member_email,
            member_address,
            partner_category,
            partner_name,
        } = req.body;

        await knex('innovation_members')
            .insert([{
                package_no,
                member_name,
                member_gender,
                member_type,
                member_identity,
                member_roles,
                member_phone,
                member_email,
                member_address,
                partner_category,
                partner_name,
                created_at: knex.fn.now()
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        response.error(res, error, 'innovation_member/innovation_member_insert');
    }
}

exports.innovation_member_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            package_no,
            member_name,
            member_gender,
            member_type,
            member_identity,
            member_roles,
            member_phone,
            member_email,
            member_address,
            partner_category,
            partner_name,
        } = req.body;

        await knex('innovation_members')
            .update({
                package_no,
                member_name,
                member_gender,
                member_type,
                member_identity,
                member_roles,
                member_phone,
                member_email,
                member_address,
                partner_category,
                partner_name,
                updated_at: knex.fn.now()
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        response.error(res, error, 'innovation_member/innovation_member_update');
    }
}

exports.innovation_member_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('innovation_members').del().where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'innovation_member/innovation_member_destroy');
    }
}