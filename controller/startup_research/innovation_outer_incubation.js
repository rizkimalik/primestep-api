'use strict';
const knex = require('../../config/connection');
const { response, logger, currency_to_number } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

exports.outer_incubation_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no } = req.body;
        const result = await knex('innovation_outer_incubation').where({ package_no }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'outer_incubation/innovation_outer_incubation_detail');
    }
}

exports.outer_incubation_insert_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            package_no,
            jumlah_produksi,
            jumlah_penjualan,
            jumlah_pendapatan,
            jumlah_profit,
            legalitas_izin_usaha,
            sertifikasi_izin_produk,
            jumlah_tenaga_kerja,
            bisnis_model,
            mitra_industri,
            supply_chain,
            target_pasar,
            pangsa_pasar,
            harga_produksi_jual,
        } = req.body;

        if (package_no) {
            const status = await knex('innovation_outer_incubation').where({ package_no }).first();
            if (status) {
                await knex('innovation_outer_incubation')
                    .update({
                        package_no,
                        jumlah_produksi: currency_to_number(jumlah_produksi),
                        jumlah_penjualan: currency_to_number(jumlah_penjualan),
                        jumlah_pendapatan: currency_to_number(jumlah_pendapatan),
                        jumlah_profit: currency_to_number(jumlah_profit),
                        legalitas_izin_usaha,
                        sertifikasi_izin_produk,
                        jumlah_tenaga_kerja: currency_to_number(jumlah_tenaga_kerja),
                        bisnis_model,
                        mitra_industri,
                        supply_chain,
                        target_pasar,
                        pangsa_pasar,
                        harga_produksi_jual,
                    })
                    .where({ package_no });

            } else {
                await knex('innovation_outer_incubation')
                    .insert([{
                        package_no,
                        jumlah_produksi: currency_to_number(jumlah_produksi),
                        jumlah_penjualan: currency_to_number(jumlah_penjualan),
                        jumlah_pendapatan: currency_to_number(jumlah_pendapatan),
                        jumlah_profit: currency_to_number(jumlah_profit),
                        legalitas_izin_usaha,
                        sertifikasi_izin_produk,
                        jumlah_tenaga_kerja: currency_to_number(jumlah_tenaga_kerja),
                        bisnis_model,
                        mitra_industri,
                        supply_chain,
                        target_pasar,
                        pangsa_pasar,
                        harga_produksi_jual,
                    }]);
            }
        }
        response.ok(res, 'success outer_incubation_insert_update');
    }
    catch (error) {
        response.error(res, error.message, 'outer_incubation/outer_incubation_insert_update');
    }
}


exports.outer_incubation_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            package_no,
            jumlah_produksi,
            jumlah_penjualan,
            jumlah_pendapatan,
            jumlah_profit,
            legalitas_izin_usaha,
            sertifikasi_izin_produk,
            jumlah_tenaga_kerja,
            bisnis_model,
            mitra_industri,
            supply_chain,
            target_pasar,
            pangsa_pasar,
            harga_produksi_jual,
        } = req.body;

        await knex('innovation_outer_incubation')
            .insert({
                package_no,
                jumlah_produksi,
                jumlah_penjualan,
                jumlah_pendapatan,
                jumlah_profit,
                legalitas_izin_usaha,
                sertifikasi_izin_produk,
                jumlah_tenaga_kerja,
                bisnis_model,
                mitra_industri,
                supply_chain,
                target_pasar,
                pangsa_pasar,
                harga_produksi_jual,
            });
        response.ok(res, 'success outer_incubation_insert');
    }
    catch (error) {
        logger('outer_incubation/outer_incubation_insert', error.message)
    }
}

exports.outer_incubation_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            id,
            package_no,
            jumlah_produksi,
            jumlah_penjualan,
            jumlah_pendapatan,
            jumlah_profit,
            legalitas_izin_usaha,
            sertifikasi_izin_produk,
            jumlah_tenaga_kerja,
            bisnis_model,
            mitra_industri,
            supply_chain,
            target_pasar,
            pangsa_pasar,
            harga_produksi_jual,
        } = req.body;

        await knex('innovation_outer_incubation')
            .update({
                package_no,
                jumlah_produksi,
                jumlah_penjualan,
                jumlah_pendapatan,
                jumlah_profit,
                legalitas_izin_usaha,
                sertifikasi_izin_produk,
                jumlah_tenaga_kerja,
                bisnis_model,
                mitra_industri,
                supply_chain,
                target_pasar,
                pangsa_pasar,
                harga_produksi_jual,
            })
            .where({ id });
        response.ok(res, 'success outer_incubation_update');
    }
    catch (error) {
        logger('outer_incubation/outer_incubation_update', error.message)
    }
}

