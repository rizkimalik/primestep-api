const startup_research = require('./startup_research');
const innovation_leader = require('./innovation_leader');
const innovation_member = require('./innovation_member');
const innovation_outer = require('./innovation_outer');
const innovation_outer_incubation = require('./innovation_outer_incubation');
const innovation_partner = require('./innovation_partner');
const status_document = require('./status_document');
const evaluator = require('./evaluator');
const history_tkt = require('./history_tkt');

module.exports =  {
    startup_research,
    innovation_leader,
    innovation_member,
    innovation_outer,
    innovation_outer_incubation,
    innovation_partner,
    status_document,
    evaluator,
    history_tkt,
}