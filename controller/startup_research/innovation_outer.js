'use strict';
const knex = require('../../config/connection');
const { response, logger, random_string } = require('../../helper');
const { upload_document_file } = require('../../helper/file_manager');
const { auth_jwt_bearer } = require('../../middleware');

//? innvovation outers
exports.innovation_outer_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, package_no } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
                SELECT * FROM innovation_outer
                WHERE package_no='${package_no}'
                ${filtering}
                ${orderby}
            `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'innovation_outer/innovation_outer_list');
    }
}

exports.innovation_outer_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.body;
        const result = await knex('innovation_outer').where({ id }).first();
        const doc = await knex('document_upload').where({ package_no: result.outer_code }).first();
        result.documentation = doc ? doc.filename : '';

        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'innovation_outer/innovation_outers_detail');
    }
}

exports.innovation_outer_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            package_no,
            institution,
            tkt_value,
            outer_code,
            outer_year,
            outer_in_year,
            outer_name,
            outer_category,
            description,
        } = req.body;

        await knex('innovation_outer')
            .insert({
                package_no,
                institution,
                tkt_value,
                outer_code,
                outer_year,
                outer_in_year,
                outer_name,
                outer_category,
                description,
            });
        response.ok(res, 'success innovation_outer_insert');
    }
    catch (error) {
        logger('innovation_outer/innovation_outer_insert', error.message)
    }
}

exports.innovation_outer_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            id,
            package_no,
            institution,
            tkt_value,
            outer_code,
            outer_year,
            outer_in_year,
            outer_name,
            outer_category,
            description,
        } = req.body;

        await knex('innovation_outer')
            .update({
                package_no,
                institution,
                tkt_value,
                outer_code,
                outer_year,
                outer_in_year,
                outer_name,
                outer_category,
                description,
            })
            .where({ id });
        response.ok(res, 'success innovation_outer_update');
    }
    catch (error) {
        logger('innovation_outer/innovation_outer_update', error.message)
    }
}

exports.innovation_outer_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('innovation_outer').del().where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'innovation_outer/innovation_outer_destroy');
    }
}


exports.innovation_outer_update_publication = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            package_no,
            outer_publication,
        } = req.body;

        await knex('innovation_outer').update({ outer_publication }).where({ package_no });
        response.ok(res, 'success innovation_outer_update');
    }
    catch (error) {
        logger('innovation_outer/innovation_outer_update', error.message)
    }
}

exports.innovation_outer_detail_publication = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no } = req.body;
        const result = await knex('innovation_outer').where({ package_no }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'innovation_outer/innovation_outer_detail_publication');
    }
}

exports.innovation_outer_docs_uploaded = async function (req, res) {
    if (req.method !== 'POST') return res.status(405).end();
    auth_jwt_bearer(req, res);
    const document_file = req.files?.document_file;
    const { outer_code } = req.body;

    if (document_file) {
        const filename = random_string(5) + '-' + document_file.name;
        await upload_document_file({ channel: 'usulan-luaran', attachment: document_file, filename });
        await knex('document_upload')
            .insert([{
                package_no: outer_code,
                document_for: 'usulan-luaran',
                document_name: 'Dokumentasi Produk (Foto/Video)',
                status: 'Luaran',
                filename,
                filesize: document_file.size,
                created_at: knex.fn.now()
            }]);
    }
    response.ok(res, 'Uploaded innovation_outer_docs_uploaded');
}
