'use strict';
const knex = require('../../config/connection');
const { response, logger, random_string } = require('../../helper');
const { upload_document_file } = require('../../helper/file_manager');
const { auth_jwt_bearer } = require('../../middleware');

//? innvovation outers
exports.innovation_partner_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, package_no } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM innovation_partners
            WHERE package_no='${package_no}'
            ${filtering}
        `);

        // const result = await knex.raw(`
        //         SELECT * FROM innovation_partners
        //         WHERE package_no='${package_no}'
        //         ${filtering}
        //         ${orderby}
        //         LIMIT ${datatake} OFFSET ${dataskip}
        //     `);
        // const total = await knex.raw(`SELECT COUNT(*) AS total from innovation_partners WHERE package_no='${package_no}' ${filtering}`);
        // response.data(res, result[0], total[0][0].total);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'innovation_partner/innovation_partner_list');
    }
}

exports.innovation_partner_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.body;
        const result = await knex('innovation_partners').where({ id }).first();
        const doc = await knex('document_upload').where({ package_no: result.partner_code }).first();
        result.document = doc ? doc.filename : '';

        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'innovation_partner/innovation_partners_detail');
    }
}

exports.innovation_partner_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            package_no,
            partner_code,
            partner_name,
            partner_institution,
            partner_email,
            partner_address,
            partner_country,
            institution,
            year,
            year_contribution_1,
            year_contribution_2,
            year_contribution_3,
        } = req.body;

        await knex('innovation_partners')
            .insert({
                package_no,
                partner_code,
                partner_name,
                partner_institution,
                partner_email,
                partner_address,
                partner_country,
                institution,
                year,
                year_contribution_1,
                year_contribution_2,
                year_contribution_3,
                created_at: knex.fn.now()
            });
        response.ok(res, 'success innovation_partner_insert');
    }
    catch (error) {
        logger('innovation_partner/innovation_partner_insert', error.message)
    }
}

exports.innovation_partner_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            id,
            package_no,
            partner_code,
            partner_name,
            partner_institution,
            partner_email,
            partner_address,
            partner_country,
            year_contribution_1,
            year_contribution_2,
            year_contribution_3,
        } = req.body;

        await knex('innovation_partners')
            .update({
                package_no,
                partner_code,
                partner_name,
                partner_institution,
                partner_email,
                partner_address,
                partner_country,
                year_contribution_1,
                year_contribution_2,
                year_contribution_3,
                updated_at: knex.fn.now()
            })
            .where({ id });
        response.ok(res, 'success innovation_partner_update');
    }
    catch (error) {
        logger('innovation_partner/innovation_partner_update', error.message)
    }
}

exports.innovation_partner_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('innovation_partners').del().where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error, 'innovation_partner/innovation_partner_destroy');
    }
}

