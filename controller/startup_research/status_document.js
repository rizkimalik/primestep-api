'use strict';
const knex = require('../../config/connection');
const { response, random_string, currency_to_number } = require('../../helper');
const { upload_document_file } = require('../../helper/file_manager');
const { auth_jwt_bearer } = require('../../middleware');

//? tahap usulan (package_no) => usulan, luaran, target, rab, dok-pendukung, konfirmasi
//? tahap verifikasi (proposal_no) => persiapan, verifikasi, kontrak, withdrawal app, pencairan, finish
exports.update_status_tahap_usulan = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no, status } = req.body;

        if (package_no) {
            const check = await knex('innovation_projects').select('status').where({ package_no }).first();
            if (status === 'Usulan') {
                if (check.status !== 'Usulan-Luaran' && check.status !== 'Usulan-Target' && check.status !== 'Usulan-RAB' && check.status !== 'Usulan-Mitra-Pendukung' && check.status !== 'Usulan-Konfirmasi') {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no, contract_no: null, recommend: null });
                }
            }
            else if (status === 'Usulan-Luaran') {
                if (check.status !== 'Usulan-RAB' && check.status !== 'Usulan-Mitra-Pendukung' && check.status !== 'Usulan-Konfirmasi') {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no, contract_no: null, recommend: null });
                }
            }
            else if (status === 'Usulan-Target') {
                if (check.status !== 'Usulan-RAB' && check.status !== 'Usulan-Mitra-Pendukung' && check.status !== 'Usulan-Konfirmasi') {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no, contract_no: null, recommend: null });
                }
            }
            else if (status === 'Usulan-RAB') {
                if (check.status !== 'Usulan-Mitra-Pendukung' && check.status !== 'Usulan-Konfirmasi') {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no, contract_no: null, recommend: null });
                }
            }
            else if (status === 'Usulan-Mitra-Pendukung') {
                if (check.status !== 'Usulan-Konfirmasi') {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no, contract_no: null, recommend: null });
                }
            }
            else if (status === 'Usulan-Konfirmasi') {
                // statsu konfirmasi
                await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no, contract_no: null, recommend: null });
            }

            response.ok(res, 'success update_status_tahap_usulan');
        }
        else {
            response.error(res, 'package number failed', 'startup_research/update_status_tahap_usulan');
        }
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/update_status_tahap_usulan');
    }
}

exports.update_status_tahap_verifikasi = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { proposal_no, status } = req.body;

        if (proposal_no) {
            const check = await knex('innovation_projects').where({ proposal_no }).first();
            if (status === 'Persiapan-Dokumen') {
                const usulan_konfirmasi = await knex('innovation_projects').where({ proposal_no, status: 'Usulan-Konfirmasi', contract_no: null });
                for (let i = 0; i < usulan_konfirmasi.length; i++) {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no: usulan_konfirmasi[i].package_no });
                }
            }
            else if (status === 'Verifikasi-Dokumen') {
                const data_list = await knex('innovation_projects').where({ proposal_no, status: 'Persiapan-Dokumen', contract_no: null });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }
            else if (status === 'Kontrak') {
                const data_list = await knex('innovation_projects').where({ proposal_no, status: 'Verifikasi-Dokumen', recommend: 1 });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }
            // else if (status === 'Withdrawal-App') {
            //     if (check.status !== 'Pencairan' && check.status !== 'Finish') {
            //         await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ proposal_no, recommend: 1 });
            //     }
            // }
            else if (status === 'Pencairan-70%') {
                const data_list = await knex('innovation_projects').where({ proposal_no, status: 'Kontrak', recommend: 1 });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }
            else if (status === 'Pencairan-30%') {
                const data_list = await knex('innovation_projects').where({ proposal_no, status: 'Pencairan-70%', recommend: 1 });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }
            else if (status === 'Finish') {
                const data_list = await knex('innovation_projects').where({ proposal_no, status: 'Pencairan-30%', recommend: 1 });
                for (let i = 0; i < data_list.length; i++) {
                    await knex('innovation_projects').update({ status, updated_at: knex.fn.now() }).where({ package_no: data_list[i].package_no });
                }
            }

            response.ok(res, 'success update_status_tahap_verifikasi');
        }
        else {
            response.error(res, 'package number failed', 'startup_research/update_status_tahap_verifikasi');
        }
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/update_status_tahap_verifikasi');
    }
}

exports.updated_status_persiapan = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { proposal_no, submodul } = req.body;

        let type = submodul === 'startup' ? 'S' : 'R';
        await knex('innovation_projects')
            .update({
                status: 'Persiapan-Dokumen',
                updated_at: knex.fn.now()
            })
            .where({ proposal_no, type });

        response.ok(res, 'success updated_status_persiapan');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/updated_status_persiapan');
    }
}

// ? proses status verifikasi proposal
exports.updated_status_verifikasi_proposal = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            package_no,
            recommend,
            recommend_comment,
            recommend_budget,
            recommend_evaluator,
        } = req.body;

        //? insert verifikasi
        await knex('history_proposal_verification')
            .insert({
                package_no,
                recommend,
                recommend_comment,
                recommend_budget: currency_to_number(recommend_budget),
                recommend_evaluator,
                created_at: knex.fn.now()
            });

        //? update paket by package_no   
        await knex('innovation_projects')
            .update({
                recommend,
                recommend_comment,
                recommend_budget: currency_to_number(recommend_budget)
            })
            .where({ package_no });

        response.ok(res, 'success updated_status_verifikasi_proposal');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/updated_status_verifikasi_proposal');
    }
}

exports.updated_status_revisi_proposal = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no } = req.body;
        await knex('innovation_projects').update({ recommend: null }).where({ package_no });
        response.ok(res, 'success updated_status_revisi_proposal');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/updated_status_revisi_proposal');
    }
}

exports.history_verifikasi_proposal = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no } = req.body;
        const result = await knex.raw(`
            SELECT * FROM history_proposal_verification WHERE package_no='${package_no}' ORDER BY id DESC
        `);
        const data = result[0];
        for (let i = 0; i < data.length; i++) {
            const user = await knex('users').select('name').where({ username: data[i].recommend_evaluator }).first();
            data[i].evaluator_name = user?.name;
        }
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/history_verifikasi_proposal');
    }
}

// ? proses status verifikasi dokumen
exports.updated_status_verifikasi_document = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            id,
            approved,
            approved_comment,
            approved_evaluator,
        } = req.body;

        await knex('document_upload')
            .update({
                approved,
                approved_comment,
                approved_evaluator,
                updated_at: knex.fn.now()
            })
            .where({ id });

        response.ok(res, 'success updated_status_verifikasi_document');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/updated_status_verifikasi_document');
    }
}

exports.updated_status_revisi_document = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.body;
        const doc = await knex('document_upload').select('status').where({ id }).first();
        if (doc) {
            await knex('document_upload').update({ status: `${doc.status}-Revisi` }).where({ id });
        }
        response.ok(res, 'success updated_status_revisi_document');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/updated_status_revisi_document');
    }
}

exports.history_verifikasi_document = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no, document_for, document_name } = req.body;
        const result = await knex.raw(`
            SELECT * FROM document_upload WHERE package_no='${package_no}' AND document_for='${document_for}' AND document_name='${document_name}' ORDER BY id DESC
        `);
        const data = result[0];
        for (let i = 0; i < data.length; i++) {
            const user = await knex('users').select('name').where({ username: data[i].approved_evaluator }).first();
            data[i].approved_name = user?.name;
        }
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/history_verifikasi_document');
    }
}


exports.updated_status_contract = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            proposal_no,
            contract_no,
            data_contract,
        } = req.body;

        await knex('innovation_projects').update({ contract_no: null }).where({ proposal_no, recommend: 1 });
        for (let i = 0; i < data_contract.length; i++) {
            await knex('innovation_projects').update({ contract_no }).where({ package_no: data_contract[i].package_no, recommend: 1 });
        }
        response.ok(res, 'success updated_status_contract');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/updated_status_contract');
    }
}

exports.updated_status_withdrawal = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            proposal_no,
            status,
        } = req.body;
        await knex('innovation_projects').update({ status }).where({ proposal_no, recommend: 1 });
        response.ok(res, 'success updated_status_withdrawal');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/updated_status_withdrawal');
    }
}


exports.updated_status_finish = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const document_file = req.files?.document_file;
        const {
            package_no,
            document_name,
            status,
            submodul,
        } = req.body;

        if (document_file) {
            const filename = random_string(5) + '-' + document_file.name;
            await upload_document_file({ channel: 'startup-research', attachment: document_file, filename });
            await knex('document_upload')
                .insert([{
                    package_no,
                    document_for: 'startup-research',
                    document_name,
                    status,
                    filename,
                    filesize: document_file.size,
                    created_at: knex.fn.now()
                }]);
        }

        if (submodul === 'startup_research') {
            await knex('innovation_projects')
                .update({
                    status: 'Complete',
                    updated_at: knex.fn.now()
                })
                .where({ package_no });
        }

        response.ok(res, 'updated_status_finish');
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/updated_status_finish');
    }
}

exports.package_status_detail = async function (req, res) {
    //> mungkin tidak dipakai func ini !!!
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no } = req.body;
        const result = await knex('package_status').where({ package_no });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/package_status_detail');
    }
}

exports.contract_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { auth, proposed_year } = req.body;

        let where_condition = '';
        if (auth.user_level === 'PIU') {
            where_condition = `WHERE deleted_at is null AND a.recommend='1' AND a.status IN ('Verifikasi-Dokumen','Kontrak','Pencairan-70%','Pencairan-30%','Finish') AND a.piu='${auth.institution}' AND a.proposed_year='${proposed_year}'`;
        }
        else {
            where_condition = `WHERE deleted_at is null AND a.recommend='1' AND a.status IN ('Verifikasi-Dokumen','Kontrak','Pencairan-70%','Pencairan-30%','Finish') AND a.proposed_year='${proposed_year}'`;
        }

        const result = await knex.raw(`
            SELECT a.piu, a.proposal_no, a.proposed_year, b.institution_name  
            FROM innovation_projects a 
            LEFT JOIN institution b on a.piu = b.institution_code
            ${where_condition}
            GROUP BY a.piu, a.proposal_no, a.proposed_year, b.institution_name
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'startup_research/contract_list');
    }
}

