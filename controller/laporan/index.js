const laporan_rnd = require('./laporan_rnd');
const laporan_kemajuan = require('./laporan_kemajuan');
const laporan_akhir = require('./laporan_akhir');
const laporan_proposal = require('./laporan_proposal');

module.exports =  {
    laporan_rnd,
    laporan_kemajuan,
    laporan_akhir,
    laporan_proposal,
}