'use strict';
const knex = require('../../config/connection');
const { response } = require('../../helper');

exports.proposal_status_review = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { modul, piu, proposed_year } = req.body;

        let where_condition = '';
        if (modul === 'research') {
            where_condition = `WHERE deleted_at is null AND type='R' AND piu='${piu}' AND proposed_year='${proposed_year}'`;
        }
        else if (modul === 'startup-incubation') {
            where_condition = `WHERE deleted_at is null AND type='S' AND jenis_usulan <> '1' AND piu='${piu}' AND proposed_year='${proposed_year}'`;
        }
        else if (modul === 'startup-acceleration') {
            where_condition = `WHERE deleted_at is null AND type='S' AND jenis_usulan='1' AND piu='${piu}' AND proposed_year='${proposed_year}'`;
        }
        const result = await knex.raw(`
            SELECT * FROM (
                SELECT package_no, proposal_name, proposed_year, recommend, evaluator, u.name as evaluator_name FROM innovation_projects
                LEFT JOIN users u ON u.username=innovation_projects.evaluator
                ${where_condition}
            ) AS alldata 
        `);
        const data = result[0];

        for (let i = 0; i < data.length; i++) {
            const total_revisi = await knex.raw(`
                SELECT COUNT(*) AS total FROM history_proposal_verification
                WHERE package_no='${data[i].package_no}' AND recommend_evaluator='${data[i].evaluator}' AND recommend='0'
            `);

            data[i].total_revisi = total_revisi[0][0].total > 0 ? (total_revisi[0][0].total - 1) : 0;
        }
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'laporan_proposal/proposal_status_review');
    }
}
