'use strict';
const knex = require('../../config/connection');
const { response } = require('../../helper');

exports.rnd_document_upload = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { modul, piu, proposed_year } = req.body;

        let where_condition, modul_document = '';
        if (modul === 'research') {
            where_condition = `WHERE deleted_at is null AND type='R' AND piu='${piu}' AND proposed_year='${proposed_year}'`;
            modul_document = 'research';
        }
        else if (modul === 'startup-incubation') {
            where_condition = `WHERE deleted_at is null AND type='S' AND jenis_usulan <> '1' AND piu='${piu}' AND proposed_year='${proposed_year}'`;
            modul_document = 'startup';
        }
        else if (modul === 'startup-acceleration') {
            where_condition = `WHERE deleted_at is null AND type='S' AND jenis_usulan='1' AND piu='${piu}' AND proposed_year='${proposed_year}'`;
            modul_document = 'startup';
        }
        const result = await knex.raw(`SELECT package_no, proposal_name, proposed_year FROM innovation_projects ${where_condition}`);
        const data = result[0];

        for (let i = 0; i < data.length; i++) {
            const dokumen_perjudul = await knex.raw(`
                SELECT COUNT(*) as total,status,modul FROM category_doc_status 
                WHERE (user_level='PIU' OR user_level IS NULL) AND upload_document=1 AND modul IN ('${modul_document}','research-startup') AND status='Usulan' GROUP BY status, modul    
            `);
            const dokumen_usulan = await knex.raw(`
                SELECT COUNT(*) as total,status,modul FROM category_doc_status 
                WHERE (user_level='PIU' OR user_level IS NULL) AND upload_document=1 AND modul IN ('${modul_document}','research-startup') AND status='Persiapan' GROUP BY status, modul    
            `);
            const dokumen_kontrak = await knex.raw(`
                SELECT COUNT(*) as total,status,modul FROM category_doc_status 
                WHERE (user_level='PIU' OR user_level IS NULL) AND upload_document=1 AND modul IN ('${modul_document}','research-startup') AND status='Kontrak' GROUP BY status, modul    
            `);
            const dokumen_pencairan = await knex.raw(`
                SELECT COUNT(*) as total,status,modul FROM category_doc_status 
                WHERE (user_level='PIU' OR user_level IS NULL) AND upload_document=1 AND modul IN ('${modul_document}','research-startup') AND status='Pencairan' GROUP BY status, modul    
            `);

            const upload_perjudul = await knex.raw(`    
                SELECT COUNT(*) AS total FROM document_upload WHERE package_no='${data[i].package_no}' AND status='Persiapan-Usulan' AND document_for IN ('${modul_document}','research-startup')    
            `);
            const upload_usulan = await knex.raw(`    
                SELECT COUNT(*) AS total FROM document_upload WHERE package_no='${data[i].proposal_no}' AND status='Persiapan-Dokumen' AND document_for IN ('${modul_document}','research-startup')    
            `);
            const upload_kontrak = await knex.raw(`    
                SELECT COUNT(*) AS total FROM document_upload WHERE package_no='${data[i].proposal_no}' AND status='Kontrak' AND document_for IN ('${modul_document}','research-startup')    
            `);
            const upload_pencairan = await knex.raw(`    
                SELECT COUNT(*) AS total FROM document_upload WHERE package_no='${data[i].proposal_no}' AND status='Pencairan' AND document_for IN ('${modul_document}','research-startup')    
            `);

            data[i].total_usulan_perjudul = upload_perjudul[0][0].total + '/' + dokumen_perjudul[0][0].total;
            data[i].total_persiapan_usulan = upload_usulan[0][0].total + '/' + dokumen_usulan[0][0].total;
            data[i].total_kontrak = upload_kontrak[0][0].total + '/' + dokumen_kontrak[0][0].total;
            data[i].total_pencairan = upload_pencairan[0][0].total + '/' + dokumen_pencairan[0][0].total;
        }
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'laporan_rnd/rnd_document_upload');
    }
}
