'use strict';
const knex = require('../../config/connection');
const { response, currency_to_number } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');

const package_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, user_level, institution, adv_year } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let where_condition = `WHERE deleted_at is null AND institution_code='${institution}' AND advertisment_year='${adv_year}'`;
        // if (user_level === 'PIU') {
        //     where_condition = `WHERE deleted_at is null AND institution_code='${institution}' AND advertisment_year='${adv_year}'`;
        // }
        // else {
        //     where_condition = `WHERE deleted_at is null AND institution_code='${institution}' AND advertisment_year='${adv_year}'`;
        // }

        const result = await knex.raw(`
            SELECT * FROM (
                SELECT a.*, b.contract_value, b.contract_vendor
                FROM package_list AS a
                LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            ) AS procurement
            ${where_condition}
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total FROM (
                SELECT a.*, b.contract_value, b.contract_vendor
                FROM package_list AS a
                LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            ) AS procurement
            ${where_condition}
            ${filtering}
        `);

        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_list');
    }
}

const information_header_status = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { institution, adv_year } = req.body;

        const where_condition = `WHERE deleted_at is null AND institution_code='${institution}' AND advertisment_year='${adv_year}'`;
        const result = await knex.raw(`
            SELECT SUM(est_value_idr) as sum_value, COUNT(*) as count_value
            FROM package_list ${where_condition}
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/information_header_status');
    }
}


const package_show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('package_list').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_show');
    }
}

const package_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            institution_code,
            advertisment_quarter,
            advertisment_year,
            // project_no,
            // package_no,
            category_code,
            general_description,
            est_value_idr,
            // est_value_jpy,
            // est_value_usd,
            procurement_method,
            review,
            type_proposal,
            advertisment_date,
            comments,
            // status,
        } = req.body;

        const zeroPad = (num, size) => String(num).padStart(size, '0');
        const { count } = await knex.count('id as count').from('package_list').where({ category_code, institution_code }).first();
        const package_no = ''.concat(category_code, '/', institution_code, '/', zeroPad(count + 1, 3))

        await knex('package_list')
            .insert([{
                package_no,
                institution_code,
                advertisment_quarter,
                advertisment_year,
                project_no: '55063-001', // no project ADB
                category_code,
                general_description,
                est_value_idr: currency_to_number(est_value_idr),
                procurement_method,
                review,
                type_proposal,
                advertisment_date,
                comments,
                status: 'Usulan',
                created_at: knex.fn.now()
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_insert');
    }
}

const package_update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            institution_code,
            advertisment_quarter,
            advertisment_year,
            // project_no,
            // package_no,
            category_code,
            general_description,
            est_value_idr,
            // est_value_jpy,
            // est_value_usd,
            procurement_method,
            review,
            type_proposal,
            advertisment_date,
            comments,
            // status,
        } = req.body;

        await knex('package_list')
            .update({
                institution_code,
                advertisment_quarter,
                advertisment_year,
                category_code,
                general_description,
                est_value_idr: currency_to_number(est_value_idr),
                procurement_method,
                review,
                type_proposal,
                advertisment_date,
                comments,
                updated_at: knex.fn.now()
            })
            .where({ id });
        response.ok(res, 'success package_update');
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_update');
    }
}

const package_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('package_list').update({ deleted_at: knex.fn.now() }).where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_destroy');
    }
}

const package_detail_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, package_no } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM package_list_detail
            WHERE deleted_at is null AND package_no='${package_no}'
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);

        const total = await knex.raw(`SELECT COUNT(*) AS total from package_list_detail WHERE deleted_at is null AND package_no='${package_no}' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_detail_list');
    }
}

const package_detail_show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('package_list_detail').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_detail_show');
    }
}

const package_detail_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            package_no,
            package_no_lot,
            general_description,
            est_value_jpy,
            est_value_idr,
            est_value_usd,
        } = req.body;

        await knex('package_list_detail')
            .insert([{
                package_no,
                package_no_lot,
                general_description,
                est_value_jpy: currency_to_number(est_value_jpy),
                est_value_idr: currency_to_number(est_value_idr),
                est_value_usd: currency_to_number(est_value_usd),
                created_at: knex.fn.now()
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_detail_insert');
    }
}

const package_detail_update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            package_no,
            package_no_lot,
            general_description,
            est_value_jpy,
            est_value_idr,
            est_value_usd,
        } = req.body;

        await knex('package_list_detail')
            .update({
                package_no,
                package_no_lot,
                general_description,
                est_value_jpy: currency_to_number(est_value_jpy),
                est_value_idr: currency_to_number(est_value_idr),
                est_value_usd: currency_to_number(est_value_usd),
                updated_at: knex.fn.now()
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_detail_update');
    }
}

const package_detail_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('package_list_detail').update({ deleted_at: knex.fn.now() }).where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_list_detail');
    }
}

const package_status_detail = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no } = req.body;
        const result = await knex('package_status').where({ package_no });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/package_status_detail');
    }
}


/**
 *  Export Data modul
 */
const export_procurement = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { institution, adv_year } = req.body;

        let where_condition = `WHERE deleted_at is null AND institution_code='${institution}' AND advertisment_year='${adv_year}'`;
        const result = await knex.raw(`
            SELECT * FROM (
                SELECT a.*, b.contract_value, b.contract_vendor
                FROM package_list AS a
                LEFT JOIN loan_contract AS b ON a.contract_no = b.contract_no
            ) AS procurement
            ${where_condition}
        `);

        response.ok(res, result[0]);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/export_procurement');
    }
}



module.exports = {
    package_list,
    information_header_status,
    package_show,
    package_insert,
    package_update,
    package_destroy,
    package_detail_list,
    package_detail_show,
    package_detail_insert,
    package_detail_update,
    package_detail_destroy,
    package_status_detail,
    export_procurement,
}