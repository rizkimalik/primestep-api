const knex = require('../../config/connection');
const { response, random_string, currency_to_number } = require('../../helper');
const { upload_document_file } = require('../../helper/file_manager');
const { auth_jwt_bearer } = require('../../middleware');


//? api document verifikasi
const update_status_packages = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no, status } = req.body;

        if (package_no) {
            const check = await knex('package_list').select('status').where({ package_no }).first();
            if (status === 'Persiapan') {
                if (check.status !== 'Pelaksanaan' && check.status !== 'Pencairan' && check.status !== 'Selesai') {
                    await knex('package_list').update({ status, updated_at: knex.fn.now() }).where({ package_no });
                }
            }
            else if (status === 'Pelaksanaan') {
                if (check.status !== 'Pencairan' && check.status !== 'Selesai') {
                    await knex('package_list').update({ status, updated_at: knex.fn.now() }).where({ package_no });
                }
            }
            else if (status === 'Pencairan') {
                if (check.status !== 'Selesai') {
                    await knex('package_list').update({ status, updated_at: knex.fn.now() }).where({ package_no });
                }
            }
            else {
                await knex('package_list').update({ status, updated_at: knex.fn.now() }).where({ package_no });
            }

            response.ok(res, 'success update_status_packages');
        }
        else {
            response.error(res, 'package number failed', 'procurement/update_status_packages');
        }
    }
    catch (error) {
        response.error(res, error.message, 'procurement/update_status_packages');
    }
}

const updated_status_planning = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const document_file = req.files?.document_file;
        const {
            package_no,
            document_name,
            status,
            description,
        } = req.body;

        if (document_file) {
            const filename = random_string(5) + '-' + document_file.name;
            await upload_document_file({ channel: 'procurement', attachment: document_file, filename });
            await knex('document_upload')
                .insert([{
                    package_no,
                    document_for: 'procurement',
                    document_name,
                    status,
                    description,
                    filename,
                    filesize: document_file.size,
                    created_at: knex.fn.now()
                }]);
        }

        response.ok(res, 'success updated_status_planning');
    }
    catch (error) {
        response.error(res, error.message, 'procurement/updated_status_planning');
    }
}

const updated_status_implementation = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            package_no,
            status,
        } = req.body;

        if (package_no) {
            // for (let i = 0; i < status.length; i++) {
            //     const isValid = await knex('package_status').where({ package_no, status: status[i].status }).first();
            //     if (isValid) {
            //         await knex('package_status')
            //             .update({
            //                 checked: status[i].checked,
            //                 updated_at: knex.fn.now()
            //             })
            //             .where({ package_no, status: status[i].status });
            //     } else {
            //         await knex('package_status')
            //             .insert({
            //                 package_no,
            //                 status: status[i].status,
            //                 checked: status[i].checked,
            //                 created_at: knex.fn.now()
            //             });
            //     }
            // }

            // const check = await knex('package_list').select('status').where({ package_no }).first();
            // if (check) {
            //     if (check.status !== 'Selesai') {
            //         await knex('package_list')
            //             .update({
            //                 status,
            //                 updated_at: knex.fn.now()
            //             })
            //             .where({ package_no });
            //     }
            // }
            response.ok(res, 'success updated_status_implementation');
        }
        else {
            response.error(res, 'package number failed', 'procurement/updated_status_implementation');
        }

    }
    catch (error) {
        response.error(res, error.message, 'procurement/updated_status_implementation');
    }
}

const updated_status_contract = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { package_no, contract_no } = req.body;

        //? no kontrak ke paket 
        await knex('package_list').update({ contract_no }).where({ package_no });
        response.ok(res, 'success updated_status_contract');
    }
    catch (error) {
        response.error(res, error.message, 'procurement/updated_status_contract');
    }
}

const updated_status_disbursement = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            package_no,
            contract_no,
            status,
            disbursement
        } = req.body;

        // if (package_no) {
        //     const status = await knex('loan_contract').where({ package_no }).first();
        //     if (status) {
        //         await knex('loan_contract')
        //             .update({
        //                 contract_no,
        //                 updated_at: knex.fn.now()
        //             })
        //             .where({ package_no });

        //     } else {
        //         await knex('loan_contract')
        //             .insert([{
        //                 package_no,
        //                 contract_no,
        //                 created_at: knex.fn.now()
        //             }]);
        //     }
        // }

        // const check = await knex('package_list').select('status').where({ package_no }).first();
        // if (check) {
        //     if (check.status !== 'Lengkap') {
        //         await knex('package_list')
        //             .update({
        //                 status,
        //                 updated_at: knex.fn.now()
        //             })
        //             .where({ package_no });
        //     }
        // }
        response.ok(res, 'success updated_status_disbursement');
    }
    catch (error) {
        response.error(res, error.message, 'procurement/updated_status_disbursement');
    }
}

const updated_status_finish = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const document_file = req.files?.document_file;
        const {
            package_no,
            document_name,
            status,
            description,
        } = req.body;

        if (document_file) {
            const filename = random_string(5) + '-' + document_file.name;
            await upload_document_file({ channel: 'procurement', attachment: document_file, filename });
            await knex('document_upload')
                .insert([{
                    package_no,
                    document_for: 'procurement',
                    document_name,
                    status,
                    description,
                    filename,
                    filesize: document_file.size,
                    created_at: knex.fn.now()
                }]);
        }

        response.ok(res, 'success updated_status_finish');
    }
    catch (error) {
        response.error(res, error.message, 'procurement/updated_status_finish');
    }
}

const insert_docs_uploaded = async function (req, res) {
    if (req.method !== 'POST') return res.status(405).end();
    auth_jwt_bearer(req, res);
    const document_file = req.files?.document_file;
    const {
        package_no,
        document_name,
        status,
        description,
    } = req.body;

    if (document_file) {
        const filename = random_string(5) + '-' + document_file.name;
        await upload_document_file({ channel: 'procurement', attachment: document_file, filename });
        await knex('document_upload')
            .insert([{
                package_no,
                document_for: 'procurement',
                document_name,
                status,
                filename,
                description,
                filesize: document_file.size,
                created_at: knex.fn.now()
            }]);
    }
    response.ok(res, 'Uploaded success');
}

const list_docs_uploaded = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, package_no, status } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 5;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM document_upload
            WHERE package_no='${package_no}' AND document_for='procurement' AND status='${status}'
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);

        const total = await knex.raw(`SELECT COUNT(*) AS total from document_upload WHERE package_no='${package_no}' AND document_for='procurement' AND status='${status}' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/list_docs_uploaded');
    }
}

const delete_document_uploaded = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('document_upload').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/delete_document_uploaded');
    }
}

const list_document_status = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const result = await knex('category_doc_status').where({ modul: 'procurement', upload_document: 1 });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'procurement/list_document_status');
    }
}

module.exports = {
    //? status & dokumen
    update_status_packages,
    updated_status_planning,
    updated_status_implementation,
    updated_status_contract,
    updated_status_disbursement,
    updated_status_finish,
    list_docs_uploaded,
    insert_docs_uploaded,
    delete_document_uploaded,
    list_document_status,
}