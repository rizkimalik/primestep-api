const package = require('./package');
const status_document = require('./status_document');

module.exports =  {
    package,
    status_document,
}
