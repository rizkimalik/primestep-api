'use strict';
const knex = require('../config/connection');
const response = require('../helper/json_response');
const { auth_jwt_bearer } = require('../middleware');

exports.dmf_data_pivot = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { submodul, performance_id, indicator_id } = req.body;
        const result = await knex('dmf_detail').select('package_no', 'indicator_id', 'institution', 'gender', 'amount', 'year', 'record_date').where({ submodul, performance_id, indicator_id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'dmf/dmf_data_pivot');
    }
}

exports.dmf_performance_header = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { submodul } = req.body;
        const result = await knex('dmf_performance').where({ submodul }).orderBy('id', 'asc');
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'dmf/dmf_performance_header');
    }
}

exports.dmf_indicator_header = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { submodul, performance_id } = req.body;
        const result = await knex('dmf_indicator').where({ submodul, performance_id }).orderBy('id', 'asc');
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'dmf/dmf_performance_header');
    }
}

exports.dmf_detail_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, submodul } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';
        let filtering = '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT a.*,b.indicator, c.performance FROM dmf_detail a
            LEFT JOIN dmf_indicator b ON a.indicator_id=b.id
            LEFT JOIN dmf_performance c ON a.performance_id=c.id
            WHERE a.submodul='${submodul}'
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total FROM dmf_detail a
            LEFT JOIN dmf_indicator b ON a.indicator_id=b.id
            LEFT JOIN dmf_performance c ON a.performance_id=c.id
            WHERE a.submodul='${submodul}'
            ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'dmf/dmf_detail_list');
    }
}

exports.dmf_detail_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            performance_id,
            indicator_id,
            submodul,
            institution,
            amount,
            gender,
            record_date,
            comments,
        } = req.body;
        const zeroPad = (num, size) => String(num).padStart(size, '0');
        const { count } = await knex.count('id as count').from('dmf_detail').where({ performance_id, institution }).first();

        let package_number = submodul.concat('/', institution, '/', zeroPad(count + 1, 3));
        await knex('dmf_detail')
            .insert([{
                performance_id,
                indicator_id,
                submodul,
                package_no: package_number,
                institution,
                amount,
                gender,
                year: new Date(record_date).getFullYear(),
                record_date,
                comments,
                created_at: knex.fn.now()
            }]);

        response.ok(res, 'success insert dmf_detail_insert');
    }
    catch (error) {
        response.error(res, error.message, 'dmf/dmf_detail_insert');
    }
}

exports.dmf_detail_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            performance_id,
            indicator_id,
            // package_no,
            institution,
            amount,
            gender,
            record_date,
            comments,
        } = req.body;

        if (record_date) {
            await knex('dmf_detail')
                .update({
                    performance_id,
                    indicator_id,
                    institution,
                    amount,
                    gender,
                    year: new Date(record_date).getFullYear(),
                    record_date: new Date(record_date),
                    comments,
                    updated_at: knex.fn.now()
                })
                .where({ id });
        } else {
            await knex('dmf_detail')
                .update({
                    performance_id,
                    indicator_id,
                    institution,
                    amount,
                    gender,
                    comments,
                    updated_at: knex.fn.now()
                })
                .where({ id });
        }

        response.ok(res, 'success dmf_detail_update');
    }
    catch (error) {
        response.error(res, error.message, 'dmf/dmf_detail_update');
    }
}

exports.dmf_detail_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('dmf_detail').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'dmf/dmf_detail_destroy');
    }
}


// ? master data DMF Performance
exports.master_dmf_performance_index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { submodul } = req.body;
        let result = '';

        if (submodul) {
            result = await knex('dmf_performance').where({ submodul });
        } else {
            result = await knex('dmf_performance');
        }
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'dmf/master_dmf_performance_index');
    }
}

// ? master data DMF Indicator
exports.master_dmf_indicator_index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { submodul, performance_id } = req.body;
        let result = '';

        if (performance_id) {
            result = await knex('dmf_indicator').where({ submodul, performance_id });
        } else {
            result = await knex('dmf_indicator').where({ submodul });
        }
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'dmf/master_dmf_indicator_index');
    }
}

exports.master_dmf_indicator_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';
        let filtering = '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM dmf_indicator
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from dmf_indicator ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'dmf/master_dmf_indicator_list');
    }
}

exports.master_dmf_indicator_show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const result = await knex('dmf_indicator').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'dmf/master_dmf_indicator_show');
    }
}

exports.master_dmf_indicator_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            performance_id,
            number,
            submodul,
            indicator,
            target_all,
            target_ipb,
            target_itb,
            target_ugm,
            target_ui,
            rate,
            comments,
        } = req.body;

        await knex('dmf_indicator')
            .insert([{
                performance_id,
                number,
                submodul,
                indicator,
                target_all,
                target_ipb,
                target_itb,
                target_ugm,
                target_ui,
                rate,
                comments,
            }]);
        response.ok(res, 'success master_dmf_indicator_insert');
    }
    catch (error) {
        response.error(res, error.message, 'dmf/master_dmf_indicator_insert');
    }
}

exports.master_dmf_indicator_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            performance_id,
            number,
            submodul,
            indicator,
            target_all,
            target_ipb,
            target_itb,
            target_ugm,
            target_ui,
            rate,
            comments,
        } = req.body;

        await knex('dmf_indicator')
            .update({
                performance_id,
                number,
                submodul,
                indicator,
                target_all,
                target_ipb,
                target_itb,
                target_ugm,
                target_ui,
                rate,
                comments,
            })
            .where({ id });
        response.ok(res, 'success update master_dmf_indicator_update');
    }
    catch (error) {
        response.error(res, error.message, 'dmf/master_dmf_indicator_update');
    }
}

exports.master_dmf_indicator_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('dmf_indicator').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'dmf/master_dmf_indicator_destroy');
    }
}