'use strict';
const knex = require('../config/connection');
const { response, random_string } = require('../helper');
const { upload_document_file } = require('../helper/file_manager');
const { auth_jwt_bearer } = require('../middleware');

exports.gender_action_pivot = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { gap_id } = req.body;
        const result = await knex('gender_action_detail').select('piu', 'year', 'gender', 'amount', 'action_date').where({ gap_id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/gender_action_pivot');
    }
}

exports.gender_action_header = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const result = await knex('gender_action_plan').orderBy('sort_code');
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/gender_action_header');
    }
}

exports.gender_action_detail_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';
        let filtering = '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT a.*,b.gender_action_plan FROM gender_action_detail a
            LEFT JOIN gender_action_plan b ON a.gap_id=b.id
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from gender_action_detail a LEFT JOIN gender_action_plan b ON a.gap_id=b.id ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/gender_action_detail_list');
    }
}

exports.gender_action_detail_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            gap_id,
            piu,
            amount,
            gender,
            comments,
            action_date,
        } = req.body;
        const document_file = req.files?.document_file;
        const zeroPad = (num, size) => String(num).padStart(size, '0');
        const { count } = await knex.count('id as count').from('gender_action_detail').where({ gap_id, piu }).first();

        let package_number = 'GAP/'.concat(piu, '/', zeroPad(count + 1, 3));
        await knex('gender_action_detail')
            .insert([{
                package_no: package_number,
                gap_id,
                piu,
                year: new Date(action_date).getFullYear(),
                amount,
                gender,
                comments,
                action_date,
                document_upload: document_file ? true : false, // check doc upload
                created_at: knex.fn.now()
            }]);

        // file data
        if (document_file) {
            const filename = random_string(5) + '-' + document_file.name;
            await upload_document_file({ channel: 'gender_action_plan', attachment: document_file, filename });
            await knex('document_upload')
                .insert([{
                    package_no: package_number,
                    document_for: 'gender_action_plan',
                    document_name: filename,
                    status: 'Complete',
                    filename,
                    filesize: document_file.size,
                    created_at: knex.fn.now()
                }]);
        }
        response.ok(res, 'success insert gender_action_detail_insert');
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/gender_action_detail_insert');
    }
}

exports.gender_action_detail_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            gap_id,
            piu,
            amount,
            gender,
            comments,
            action_date,
        } = req.body;

        if (action_date) {
            await knex('gender_action_detail')
                .update({
                    gap_id,
                    piu,
                    year: new Date(action_date).getFullYear(),
                    amount,
                    gender,
                    comments,
                    action_date: new Date(action_date),
                    updated_at: knex.fn.now()
                })
                .where({ id });
        } else {
            await knex('gender_action_detail')
                .update({
                    gap_id,
                    piu,
                    amount,
                    gender,
                    comments,
                    updated_at: knex.fn.now()
                })
                .where({ id });
        }

        response.ok(res, 'success insert gender_action_detail_update');
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/gender_action_detail_update');
    }
}

exports.gender_action_detail_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('gender_action_detail').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/gender_action_detail_destroy');
    }
}

exports.gender_action_file_upload = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const document_file = req.files?.document_file;
        const {
            package_no,
            status,
        } = req.body;

        if (document_file) {
            const filename = random_string(5) + '-' + document_file.name;
            await upload_document_file({ channel: 'gender_action_plan', attachment: document_file, filename });
            await knex('document_upload')
                .insert([{
                    package_no,
                    document_for: 'gender_action_plan',
                    document_name: filename,
                    status,
                    filename,
                    filesize: document_file.size,
                    created_at: knex.fn.now()
                }]);
        }
        response.ok(res, 'Document Uploaded success');
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/gender_action_file_upload');
    }
}

exports.gap_file_upload_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, package_no } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 5;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM document_upload
            WHERE package_no='${package_no}' AND document_for='gender_action_plan'
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);

        const total = await knex.raw(`SELECT COUNT(*) AS total from document_upload WHERE package_no='${package_no}' AND document_for='gender_action_plan' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/gap_file_upload_list');
    }
}

exports.gap_file_upload_delete = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('document_upload').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/gap_file_upload_delete');
    }
}

// ? master data gender_action_plan
exports.master_gender_action_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';
        let filtering = '';

        let orderby = 'ORDER BY id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM gender_action_plan
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from gender_action_plan ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/gender_action_plan');
    }
}
exports.master_gender_action_show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('gender_action_plan').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/master_gender_action_show');
    }
}

exports.master_gender_action_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            sort_code,
            target_percentase,
            group_name,
            gender_action_plan,
        } = req.body;

        await knex('gender_action_plan')
            .insert([{
                sort_code,
                target_percentase,
                group_name,
                gender_action_plan,
                created_at: knex.fn.now()
            }]);
        response.ok(res, 'success insert gender_action_plan');
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/master_gender_action_insert');
    }
}

exports.master_gender_action_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            sort_code,
            target_percentase,
            group_name,
            gender_action_plan,
        } = req.body;

        await knex('gender_action_plan')
            .update({
                sort_code,
                target_percentase,
                group_name,
                gender_action_plan,
                updated_at: knex.fn.now()
            })
            .where({ id });
        response.ok(res, 'success update gender_action_plan');
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/master_gender_action_update');
    }
}

exports.master_gender_action_destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('gender_action_plan').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'gender_action/master_gender_action_destroy');
    }
}