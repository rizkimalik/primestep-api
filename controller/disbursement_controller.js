'use strict';
const knex = require('../config/connection');
const { response, currency_to_number } = require('../helper');
const { auth_jwt_bearer } = require('../middleware');

//? crud
exports.disbursement_list = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { contract_no } = req.body;
        const result = await knex('disbursement_fund').where({ contract_no });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'disbursement/disbursement_list');
    }
}

exports.disbursement_show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('disbursement_fund').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'disbursement/disbursement_show');
    }
}

exports.disbursement_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const {
            contract_no,
            institution,
            disbursement_value,
            disbursement_percent,
            disbursement_date,
        } = req.body;

        const result = await knex('disbursement_fund')
            .insert({
                contract_no,
                institution,
                disbursement_value: currency_to_number(disbursement_value),
                disbursement_percent,
                disbursement_date: new Date(disbursement_date),
            });

        response.ok(res, result[0].message);
    }
    catch (error) {
        response.error(res, error.message, 'disbursement/disbursement_insert');
    }
}


exports.disbursement_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            disbursement_value,
            disbursement_percent,
            disbursement_date,
        } = req.body;

        let values = '';
        if (disbursement_date) {
            values = {
                disbursement_value: currency_to_number(disbursement_value),
                disbursement_percent,
                disbursement_date: new Date(disbursement_date),
            }
        }
        else {
            values = {
                disbursement_value: currency_to_number(disbursement_value),
                disbursement_percent,
            }
        }

        const result = await knex('disbursement_fund').update(values).where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'disbursement/disbursement_update');
    }
}


exports.disbursement_delete = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('disbursement_fund').where({ id }).del()
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'disbursement/disbursement_delete');
    }
}