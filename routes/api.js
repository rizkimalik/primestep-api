'use strict';
const { file_manager } = require('../helper');
const auth = require('../controller/auth_controller');
const menu = require('../controller/menu_controller');
const user = require('../controller/users_controller');
const dashboard = require('../controller/dashboard');
const master = require('../controller/master_data');
const contract = require('../controller/contract_controller');
const disbursement = require('../controller/disbursement_controller');
const document = require('../controller/document_upload_controller');
const procurement = require('../controller/procurement');
const dmf = require('../controller/dmf_controller');
const strengthen_capability = require('../controller/strengthen_capability');
const startup_research = require('../controller/startup_research');
const issues_challenges = require('../controller/issues_challenges_controller');
const gender_action = require('../controller/gender_action_controller');
const rab = require('../controller/rab_controller');
const notification = require('../controller/notification_controller');
const laporan = require('../controller/laporan');

module.exports = function (app) {
    app.route('/').get(function (req, res) {
        res.json({ message: "Application API running! 🤘🚀" });
        res.end();
    });

    //? read file logger
    app.route('/logs/:date').get(function (req, res) {
        try {
            const { date } = req.params;
            const log = file_manager.read_dir_file(`./logs/${date}`);
            res.json(log);
            res.end();
        }
        catch (error) {
            res.json(error.message);
            res.end();
        }
    });

    app.prefix('/menu', function (api) {
        api.route('/main_menu/:user_level').get(menu.main_menu);
        api.route('/menu').get(menu.menu);
        api.route('/menu_modul/:menu_id').get(menu.menu_modul);
        api.route('/menu_submodul/:menu_modul_id').get(menu.menu_submodul);
        api.route('/menu_access').get(menu.menu_access);
        api.route('/modul_access').get(menu.modul_access);
        api.route('/submodul_access').get(menu.submodul_access);
        api.route('/store_access').post(menu.store_access);
        api.route('/delete_access/:id').delete(menu.delete_access);
    });

    app.prefix('/auth', function (api) {
        api.route('/login').post(auth.login);
        api.route('/logout').post(auth.logout);
        api.route('/check_auth_expired').post(auth.check_auth_expired);
    });
    
    app.prefix('/notification', function (api) {
        api.route('/proposal').post(notification.notification_proposal);
        api.route('/document').post(notification.notification_document);
    });

    app.prefix('/user', function (api) {
        api.route('/').post(user.index);
        api.route('/show/:id').get(user.show);
        api.route('/store').post(user.store);
        api.route('/update').put(user.update);
        api.route('/reset_password').put(user.reset_password);
        api.route('/delete/:id').delete(user.destroy);
    });

    app.prefix('/dashboard', function (api) {
        api.route('/total/disbursed_and_packages').post(dashboard.total_progress.total_disbursed_and_packages);
        api.route('/total/procurement_progress').post(dashboard.total_progress.total_procurement_progress);
        api.route('/loan_milestone').post(dashboard.loan_milestone.loan_milestone);
        api.route('/loan_progress_permonth').post(dashboard.loan_milestone.loan_progress_permonth);
        api.route('/evaluator/total').post(dashboard.dash_evaluator.total_packages);
        api.route('/evaluator/data').post(dashboard.dash_evaluator.data_packages);
    });
    
    app.prefix('/contract', function (api) {
        api.route('/detail').post(contract.contract_detail);
        api.route('/update').post(contract.contract_update);
    });
    
    app.prefix('/disbursement', function (api) {
        api.route('/list').post(disbursement.disbursement_list);
        api.route('/show/:id').get(disbursement.disbursement_show);
        api.route('/insert').post(disbursement.disbursement_insert);
        api.route('/update').post(disbursement.disbursement_update);
        api.route('/delete/:id').delete(disbursement.disbursement_delete);
    });
    
    app.prefix('/document', function (api) {
        api.route('/list').post(document.list_docs_uploaded);
        api.route('/upload').post(document.insert_docs_upload);
        api.route('/update').post(document.update_docs_upload);
        api.route('/delete/:id').delete(document.delete_document_upload);
    });

    app.prefix('/procurement', function (api) {
        api.route('/package/list').post(procurement.package.package_list);
        api.route('/package/information/status').post(procurement.package.information_header_status);
        api.route('/package/show/:id').get(procurement.package.package_show);
        api.route('/package/insert').post(procurement.package.package_insert);
        api.route('/package/update').put(procurement.package.package_update);
        api.route('/package/delete/:id').delete(procurement.package.package_destroy);
        api.route('/package/detail_list').post(procurement.package.package_detail_list);
        api.route('/package/detail_show/:id').get(procurement.package.package_detail_show);
        api.route('/package/detail_insert').post(procurement.package.package_detail_insert);
        api.route('/package/detail_update').put(procurement.package.package_detail_update);
        api.route('/package/detail_delete/:id').delete(procurement.package.package_detail_destroy);
        api.route('/package/status/detail').post(procurement.package.package_status_detail);
        //? status & dokumen
        api.route('/package/update/status').post(procurement.status_document.update_status_packages);
        api.route('/package/update/planning').post(procurement.status_document.updated_status_planning);
        api.route('/package/update/implementation').post(procurement.status_document.updated_status_implementation);
        api.route('/package/update/contract').post(procurement.status_document.updated_status_contract);
        api.route('/package/update/disbursement').post(procurement.status_document.updated_status_disbursement);
        api.route('/package/update/finish').post(procurement.status_document.updated_status_finish);
        api.route('/package/docs/uploaded/insert').post(procurement.status_document.insert_docs_uploaded);
        api.route('/package/docs/uploaded').post(procurement.status_document.list_docs_uploaded);
        api.route('/package/docs/delete/:id').delete(procurement.status_document.delete_document_uploaded);
        api.route('/package/docs/status').post(procurement.status_document.list_document_status);

        api.route('/export/excel').post(procurement.package.export_procurement);
    });

    app.prefix('/strengthen_capability', function (api) {
        api.route('/list').post(strengthen_capability.strengthen_capability.strengthen_capability_list);
        api.route('/information/status').post(strengthen_capability.strengthen_capability.information_header_status);
        api.route('/show/:id').get(strengthen_capability.strengthen_capability.strengthen_capability_show);
        api.route('/insert').post(strengthen_capability.strengthen_capability.strengthen_capability_insert);
        api.route('/update').post(strengthen_capability.strengthen_capability.strengthen_capability_update);
        api.route('/delete/:id').delete(strengthen_capability.strengthen_capability.strengthen_capability_destroy);

        api.route('/update/status/progress').post(strengthen_capability.status_document.update_status_progress);
        api.route('/status/verifikasi/document').post(strengthen_capability.status_document.updated_status_verifikasi_document);
        api.route('/status/revisi/document').post(strengthen_capability.status_document.updated_status_revisi_document);
        api.route('/history/verification/document').post(strengthen_capability.status_document.history_verifikasi_document);
        api.route('/status/update/contract').post(strengthen_capability.status_document.updated_status_contract);
        api.route('/status/update/withdrawal').post(strengthen_capability.status_document.updated_status_withdrawal);
        api.route('/check/verification/document').post(strengthen_capability.status_document.check_verification_document);
        
        api.route('/dmf/indicator').post(strengthen_capability.strengthen_capability.dmf_indicator_list);
        api.route('/disbursement/perquarter').post(strengthen_capability.strengthen_capability.sum_disbursement_perquarter);
        api.route('/verified/list').post(strengthen_capability.strengthen_capability.strengthen_capability_verified);
        api.route('/contract/list').post(strengthen_capability.strengthen_capability.contract_list);
        api.route('/contract/detail').post(strengthen_capability.status_document.contract_detail);
        api.route('/status/detail').post(strengthen_capability.status_document.package_status_detail);

        api.route('/export/excel').post(strengthen_capability.strengthen_capability.export_strengthen_capability);
    });

    app.prefix('/strengthen_pnbp', function (api) {
        api.route('/list').post(strengthen_capability.strengthen_pnbp.strengthen_pnbp_list);
        api.route('/information/status').post(strengthen_capability.strengthen_pnbp.information_header_status);
        api.route('/show/:id').get(strengthen_capability.strengthen_pnbp.strengthen_pnbp_show);
        api.route('/insert').post(strengthen_capability.strengthen_pnbp.strengthen_pnbp_insert);
        api.route('/update').post(strengthen_capability.strengthen_pnbp.strengthen_pnbp_update);
        api.route('/delete/:id').delete(strengthen_capability.strengthen_pnbp.strengthen_pnbp_destroy);

        api.route('/dmf/indicator').post(strengthen_capability.strengthen_pnbp.dmf_indicator_list);
        api.route('/export/excel').post(strengthen_capability.strengthen_pnbp.export_strengthen_pnbp);
    });

    app.prefix('/startup_research', function (api) {
        api.route('/startup/list').post(startup_research.startup_research.startup_list);
        api.route('/research/list').post(startup_research.startup_research.research_list);
        api.route('/startup/acceleration').post(startup_research.startup_research.startup_acceleration);
        api.route('/select/acceleration').post(startup_research.startup_research.startup_acceleration_select);
        api.route('/information/status').post(startup_research.startup_research.information_header_status);
        api.route('/show/:id').get(startup_research.startup_research.startup_research_show);
        api.route('/detail').post(startup_research.startup_research.startup_research_detail);
        api.route('/insert').post(startup_research.startup_research.startup_research_insert);
        api.route('/update').post(startup_research.startup_research.startup_research_update);
        api.route('/delete/:id').delete(startup_research.startup_research.startup_research_delete);
        api.route('/search/usulan').post(startup_research.startup_research.research_usulan_list);
        api.route('/recommended/list').post(startup_research.startup_research.list_packages_recommended);
        api.route('/check/verification/proposal').post(startup_research.startup_research.check_verification_proposal);
        api.route('/check/verification/document').post(startup_research.startup_research.check_verification_document);
        
        api.route('/assign/evaluator').post(startup_research.evaluator.set_assign_evaluator);
        api.route('/step/update/target').post(startup_research.startup_research.step_update_target);
        api.route('/status/tahap_usulan').post(startup_research.status_document.update_status_tahap_usulan);
        api.route('/status/tahap_verifikasi').post(startup_research.status_document.update_status_tahap_verifikasi);

        api.route('/status/update/persiapan').post(startup_research.status_document.updated_status_persiapan);
        api.route('/status/verifikasi/document').post(startup_research.status_document.updated_status_verifikasi_document);
        api.route('/status/verifikasi/proposal').post(startup_research.status_document.updated_status_verifikasi_proposal);
        api.route('/status/revisi/document').post(startup_research.status_document.updated_status_revisi_document);
        api.route('/status/revisi/proposal').post(startup_research.status_document.updated_status_revisi_proposal);
        api.route('/history/verification/document').post(startup_research.status_document.history_verifikasi_document);
        api.route('/history/verification/proposal').post(startup_research.status_document.history_verifikasi_proposal);
        api.route('/status/update/contract').post(startup_research.status_document.updated_status_contract);
        api.route('/status/update/withdrawal').post(startup_research.status_document.updated_status_withdrawal);
        api.route('/status/update/finish').post(startup_research.status_document.updated_status_finish);
        api.route('/status/detail').post(startup_research.status_document.package_status_detail);
        api.route('/contract/list').post(startup_research.status_document.contract_list);

        api.route('/innovation_member/list').post(startup_research.innovation_member.innovation_member_list);
        api.route('/innovation_member/detail').post(startup_research.innovation_member.innovation_member_detail);
        api.route('/innovation_member/insert').post(startup_research.innovation_member.innovation_member_insert);
        api.route('/innovation_member/update').post(startup_research.innovation_member.innovation_member_update);
        api.route('/innovation_member/delete/:id').delete(startup_research.innovation_member.innovation_member_destroy);

        api.route('/innovation_outer/list').post(startup_research.innovation_outer.innovation_outer_list);
        api.route('/innovation_outer/detail').post(startup_research.innovation_outer.innovation_outer_detail);
        api.route('/innovation_outer/insert').post(startup_research.innovation_outer.innovation_outer_insert);
        api.route('/innovation_outer/update').post(startup_research.innovation_outer.innovation_outer_update);
        api.route('/innovation_outer/delete/:id').delete(startup_research.innovation_outer.innovation_outer_destroy);
        api.route('/innovation_outer/update/publication').post(startup_research.innovation_outer.innovation_outer_update_publication);
        api.route('/innovation_outer/detail/publication').post(startup_research.innovation_outer.innovation_outer_detail_publication);
        api.route('/innovation_outer/document/upload').post(startup_research.innovation_outer.innovation_outer_docs_uploaded);

        api.route('/outer_incubation/detail').post(startup_research.innovation_outer_incubation.outer_incubation_detail);
        api.route('/outer_incubation/insert_update').post(startup_research.innovation_outer_incubation.outer_incubation_insert_update);

        api.route('/innovation_partner/list').post(startup_research.innovation_partner.innovation_partner_list);
        api.route('/innovation_partner/detail').post(startup_research.innovation_partner.innovation_partner_detail);
        api.route('/innovation_partner/insert').post(startup_research.innovation_partner.innovation_partner_insert);
        api.route('/innovation_partner/update').post(startup_research.innovation_partner.innovation_partner_update);
        api.route('/innovation_partner/delete/:id').delete(startup_research.innovation_partner.innovation_partner_destroy);

        api.route('/history_tkt/insert').post(startup_research.history_tkt.history_tkt_insert);
        api.route('/history_tkt/list').post(startup_research.history_tkt.history_tkt_list);

        api.route('/export/excel').post(startup_research.startup_research.export_research_development);
    });

    app.prefix('/rab', function (api) {
        api.route('/list').post(rab.rab_list);
        api.route('/detail').post(rab.rab_detail);
        api.route('/update').post(rab.rab_update);
        api.route('/delete/:id').delete(rab.rab_destroy);
        api.route('/document/upload').post(rab.upload_excel_rab);
    });

    app.prefix('/dmf', function (api) {
        api.route('/data/pivot').post(dmf.dmf_data_pivot);
        api.route('/performance/header').post(dmf.dmf_performance_header);
        api.route('/indicator/header').post(dmf.dmf_indicator_header);
        api.route('/detail/list').post(dmf.dmf_detail_list);
        api.route('/detail/insert').post(dmf.dmf_detail_insert);
        api.route('/detail/update').post(dmf.dmf_detail_update);
        api.route('/detail/delete/:id').delete(dmf.dmf_detail_destroy);
        //? master data
        api.route('/indicator/list').post(dmf.master_dmf_indicator_list);
        api.route('/indicator/show/:id').get(dmf.master_dmf_indicator_show);
        api.route('/indicator/insert').post(dmf.master_dmf_indicator_insert);
        api.route('/indicator/update').post(dmf.master_dmf_indicator_update);
        api.route('/indicator/delete/:id').delete(dmf.master_dmf_indicator_destroy);
    });

    app.prefix('/gender_action', function (api) {
        api.route('/pivot').post(gender_action.gender_action_pivot);
        api.route('/header').post(gender_action.gender_action_header);
        api.route('/detail/list').post(gender_action.gender_action_detail_list);
        api.route('/detail/insert').post(gender_action.gender_action_detail_insert);
        api.route('/detail/update').post(gender_action.gender_action_detail_update);
        api.route('/detail/delete/:id').delete(gender_action.gender_action_detail_destroy);
        //? file data
        api.route('/file/list').post(gender_action.gap_file_upload_list);
        api.route('/file/upload').post(gender_action.gender_action_file_upload);
        api.route('/file/delete/:id').delete(gender_action.gap_file_upload_delete);
        //? master data
        api.route('/list').post(gender_action.master_gender_action_list);
        api.route('/show/:id').get(gender_action.master_gender_action_show);
        api.route('/insert').post(gender_action.master_gender_action_insert);
        api.route('/update').post(gender_action.master_gender_action_update);
        api.route('/delete/:id').delete(gender_action.master_gender_action_destroy);
    });
    
    app.prefix('/issues_challenges', function (api) {
        api.route('/list').post(issues_challenges.issues_challenges_list);
        api.route('/show/:id').get(issues_challenges.issues_challenges_show);
        api.route('/insert').post(issues_challenges.issues_challenges_insert);
        api.route('/update').put(issues_challenges.issues_challenges_update);
        api.route('/delete/:id').delete(issues_challenges.issues_challenges_destroy);
    });

    app.prefix('/master', function (api) {
        api.route('/user_level').get(master.user_level.index);
        api.route('/institution').get(master.institution.index);
        api.route('/category_package').get(master.category_package.index);
        api.route('/category_doc_status').post(master.category_doc_status.index);
        api.route('/docs_uploaded_list').post(master.category_doc_status.docs_uploaded_list);
        api.route('/category_proc_method').post(master.procurement_method.index);
        api.route('/category_proc_review').post(master.procurement_review.index);
        api.route('/category_focus').post(master.category_focus.index);
        api.route('/category_framework').post(master.category_framework.index);
        api.route('/dmf/performance').post(dmf.master_dmf_performance_index);
        api.route('/dmf/indicator').post(dmf.master_dmf_indicator_index);
        api.route('/tkt/category').post(master.indicator_tkt.category_tkt_index);
        api.route('/tkt/indicator').post(master.indicator_tkt.indicator_tkt_index);
        api.route('/strengthen/component').post(master.strengthen_component.strengthen_component_index);
        api.route('/strengthen/category').post(master.strengthen_category.strengthen_category_index);
        api.route('/pagu').post(master.pagu_contract.pagu_contract_index);
    });

    app.prefix('/institution', function (api) {
        api.route('/').post(master.institution.list);
        api.route('/show/:id').get(master.institution.show);
        api.route('/store').post(master.institution.store);
        api.route('/update').post(master.institution.update);
        api.route('/delete/:id').delete(master.institution.destroy);
    });

    app.prefix('/category_package', function (api) {
        api.route('/').post(master.category_package.category_package_list);
        api.route('/insert').post(master.category_package.category_package_insert);
        api.route('/update').post(master.category_package.category_package_update);
        api.route('/delete/:id').delete(master.category_package.category_package_destroy);
    });
    app.prefix('/document_status', function (api) {
        api.route('/').post(master.category_doc_status.doc_status_list);
        api.route('/show/:id').get(master.category_doc_status.doc_status_detail);
        api.route('/insert').post(master.category_doc_status.doc_status_insert);
        api.route('/update').post(master.category_doc_status.doc_status_update);
        api.route('/delete/:id').delete(master.category_doc_status.doc_status_destroy);
    });
    app.prefix('/procurement_method', function (api) {
        api.route('/').post(master.procurement_method.procurement_method_list);
        api.route('/insert').post(master.procurement_method.procurement_method_insert);
        api.route('/update').post(master.procurement_method.procurement_method_update);
        api.route('/delete/:id').delete(master.procurement_method.procurement_method_destroy);
    });
    app.prefix('/procurement_review', function (api) {
        api.route('/').post(master.procurement_review.procurement_review_list);
        api.route('/insert').post(master.procurement_review.procurement_review_insert);
        api.route('/update').post(master.procurement_review.procurement_review_update);
        api.route('/delete/:id').delete(master.procurement_review.procurement_review_destroy);
    });
    app.prefix('/category_focus', function (api) {
        api.route('/').post(master.category_focus.category_focus_list);
        api.route('/insert').post(master.category_focus.category_focus_insert);
        api.route('/update').post(master.category_focus.category_focus_update);
        api.route('/delete/:id').delete(master.category_focus.category_focus_destroy);
    });
    app.prefix('/category_framework', function (api) {
        api.route('/').post(master.category_framework.category_frameworklist);
        api.route('/insert').post(master.category_framework.category_framework_insert);
        api.route('/update').post(master.category_framework.category_framework_update);
        api.route('/delete/:id').delete(master.category_framework.category_framework_destroy);
    });
    app.prefix('/evaluator', function (api) {
        api.route('/list').post(startup_research.evaluator.evaluator_list);
        api.route('/detail').post(startup_research.evaluator.evaluator_detail);
        api.route('/insert').post(startup_research.evaluator.evaluator_insert);
        api.route('/update').post(startup_research.evaluator.evaluator_update);
        api.route('/delete/:id').delete(startup_research.evaluator.evaluator_delete);
    });
    app.prefix('/strengthen_component', function (api) {
        api.route('/list').post(master.strengthen_component.strengthen_component_list);
        api.route('/insert').post(master.strengthen_component.strengthen_component_insert);
        api.route('/update').post(master.strengthen_component.strengthen_component_update);
        api.route('/delete/:id').delete(master.strengthen_component.strengthen_component_destroy);
    });
    app.prefix('/strengthen_category', function (api) {
        api.route('/list').post(master.strengthen_category.strengthen_category_list);
        api.route('/insert').post(master.strengthen_category.strengthen_category_insert);
        api.route('/update').post(master.strengthen_category.strengthen_category_update);
        api.route('/delete/:id').delete(master.strengthen_category.strengthen_category_destroy);
    });
    app.prefix('/pagu', function (api) {
        api.route('/list').post(master.pagu_contract.pagu_contract_list);
        api.route('/insert').post(master.pagu_contract.pagu_contract_insert);
        api.route('/update').post(master.pagu_contract.pagu_contract_update);
        api.route('/delete/:id').delete(master.pagu_contract.pagu_contract_destroy);
    });
    
    /**
     *  LAPORAN Dokumen Upload
     */
    app.prefix('/laporan', function (api) {
        api.route('/rnd').post(laporan.laporan_rnd.rnd_document_upload);
        api.route('/proposal').post(laporan.laporan_proposal.proposal_status_review);
        // api.route('/insert').post(master.pagu_contract.pagu_contract_insert);
    });


}
