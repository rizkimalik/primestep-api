const slugify = (text) => {
    // Convert the string to lowercase.
    text = text.toLowerCase();
    // Remove all non-word characters, except hyphens and spaces.
    text = text.replace(/[^a-zA-Z0-9-\s]/g, '');
    // Replace all spaces with hyphens.
    text = text.replace(/\s+/g, '-');
    // Remove any leading or trailing hyphens.
    text = text.trim('-');

    return text;
}

module.exports = slugify;