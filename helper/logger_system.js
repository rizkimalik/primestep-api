const log4js = require('log4js')

const logger_system = (req, res) => {
    if(req.method === 'OPTIONS') return '';
    
    const result = {
        request: {
            headers: req.headers,
            host: req.headers.host,
            baseUrl: req.baseUrl,
            url: req.url,
            method: req.method,
            body: req.body,
            params: req?.params,
            query: req?.query,
            clientIp: req?.socket.remoteAddress,
        },
        response: {
            headers: res.getHeaders(),
            statusCode: res.statusCode,
            body: '',
        }
    };

    if (!log4js.isConfigured()) {
        log4js.configure({
            appenders: {
                api_system: {
                    type: "file",
                    filename: process.env.LOG_NAME,
                    maxLogSize: process.env.LOG_MAX_SIZE,
                    backups: process.env.LOG_BACKUP_NUMBER,
                }
            },
            categories: {
                default: { appenders: ['api_system'], level: 'debug' }
            },
        });
    }
    const loggers = log4js.getLogger('api_system');
    loggers.debug(`[${req.baseUrl}${req.url}] = ${JSON.stringify(result)}`);
    loggers.debug(`----------------------------------------------------------------------------------------------------------`);
}

module.exports = logger_system;
