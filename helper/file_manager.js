"use strict";
const fs = require('fs');
const https = require('https');
const logger = require('./logger');

const DownloadFileURL = async function ({ channel, file_name, file_url }) {
    try {
        const directory = `./${process.env.ATTACHMENT_DIR}/${channel}/`;
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }
        const destination = `${directory + file_name}`;

        https.get(file_url, (res) => {
            const filePath = fs.createWriteStream(destination);
            res.pipe(filePath);
            filePath.on('finish', () => {
                filePath.close();
                console.log(`Download attachment ${channel} completed.`);
                return destination.replace('./', '');
            })
        }).on('error', error => {
            fs.unlink(destination);
            logger('file_manager/DownloadFileURL', `Gagal mendownload file: ${error.message}`);

        });
        return destination.replace('./', '');

    } catch (error) {
        console.log(error.message)
        logger('file_manager/DownloadFileURL', error.message);
    }
}

const upload_document_file = async function (value) {
    // logger('file_manager/upload_document_file', value);
    const { channel, attachment, filename } = value;

    try {
        const directory = `./${process.env.ATTACHMENT_DIR}/${channel}/`;
        const url = `${directory + filename}`;

        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }
        const bufferData = Buffer.from(attachment.data, 'utf-8');
        fs.writeFileSync(url, bufferData, function (error) {
            if (error) {
                logger('file_manager/upload_document_file', error.message);
                return console.log(error.message);
            }
        });
        logger('file_manager/upload_document_file', url);

        return url;
    } catch (error) {
        console.log(error.message)
        logger('file_manager/upload_document_file', error.message);
    }
}

const DownloadFromBase64 = async function (value) {
    const { channel, message_type, message_id, message_raw } = value;
    try {
        const directory = `./${process.env.ATTACHMENT_DIR}/${channel}/`;
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }

        let dataBase64 = '';
        let fileName = '';
        if (message_type === 'image') {
            fileName = message_id + '.jpeg';
            dataBase64 = message_raw.imageMessage.jpegThumbnail;
        }
        const url = `${directory + fileName}`;

        fs.writeFile(url, dataBase64, 'base64', function (err) {
            if (err) {
                logger('file_manager/DownloadAttachment', err);
                return console.log(err);
            }
        });

        return url;
    } catch (error) {
        console.log(error)
        logger('file_manager/DownloadAttachment', error);
    }
}

function read_dir_file(dir, files_) {
    files_ = files_ || [];
    var files = fs.readdirSync(dir);
    for (var i in files) {
        var name = dir + '/' + files[i];
        if (fs.statSync(name).isDirectory()) {
            read_dir_file(name, files_);
        } else {
            files_.push(name);
        }
    }
    return files_;
}

// DownloadFileURL('Call','halo.wav','https://pabx.keurais.com:8887/api/files/676473928905916416/data')

module.exports = {
    DownloadFileURL,
    upload_document_file,
    DownloadFromBase64,
    read_dir_file,
}